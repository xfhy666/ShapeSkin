package com.xfhy.skin;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.SwitchCompat;
import android.view.View;

import skin.support.content.res.SkinCompatUserThemeManager;

public class SecondActivity extends AppCompatActivity {

    boolean isCLYH = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);


        findViewById(R.id.btn_test2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCLYH = !isCLYH;
                SwitchCompat switchCompat = findViewById(R.id.switch1);
                setSwitchColor(switchCompat);
                Drawable background = switchCompat.getBackground();


                if (isCLYH) {
                    SkinCompatUserThemeManager.get().addColorState(R.color.colorPrimary, "#3F51B5");
                    SkinCompatUserThemeManager.get().addColorState(R.color.colorAccent, "#FF4081");
                    SkinCompatUserThemeManager.get().addColorState(R.color.colorPrimaryDark, "#303F9F");

                    SkinCompatUserThemeManager.get().addColorState(R.color.normal, "#decf25");
                    SkinCompatUserThemeManager.get().addColorState(R.color.pressed, "#3fcba6");

                    SkinCompatUserThemeManager.get().addColorState(R.color.text_color_normal, "#8bde25");
                    SkinCompatUserThemeManager.get().addColorState(R.color.text_color_pressed, "#d00cef");

                    SkinCompatUserThemeManager.get().addColorState(R.color.title_checked_text_color,
                            "#3F51B5");
                    SkinCompatUserThemeManager.get().addColorState(R.color.title_unchecked_text_color,
                            "#F0F0F0");
                    SkinCompatUserThemeManager.get().addColorState(R.color.title_checked_background_color,
                            "#FFFFFF");
                    SkinCompatUserThemeManager.get().addColorState(R.color
                            .title_unchecked_background_color, "#3F51B5");
                } else {
                    SkinCompatUserThemeManager.get().addColorState(R.color.colorPrimary, "#40FF6D");
                    SkinCompatUserThemeManager.get().addColorState(R.color.colorAccent, "#40FF6D");
                    SkinCompatUserThemeManager.get().addColorState(R.color.colorPrimaryDark, "#40FF6D");
                    SkinCompatUserThemeManager.get().addColorState(R.color.normal, "#FF0000");
                    SkinCompatUserThemeManager.get().addColorState(R.color.pressed, "#ffffff");
                    SkinCompatUserThemeManager.get().addColorState(R.color.text_color_normal, "#ffffff");
                    SkinCompatUserThemeManager.get().addColorState(R.color.text_color_pressed, "#ff0000");
                    SkinCompatUserThemeManager.get().addColorState(R.color.title_checked_text_color,
                            "#FF0000");

                    SkinCompatUserThemeManager.get().addColorState(R.color.title_checked_text_color,
                            "#decf25");
                    SkinCompatUserThemeManager.get().addColorState(R.color.title_unchecked_text_color,
                            "#3fcba6");
                    SkinCompatUserThemeManager.get().addColorState(R.color.title_checked_background_color,
                            "#8bde25");
                    SkinCompatUserThemeManager.get().addColorState(R.color
                            .title_unchecked_background_color, "#d00cef");
                }
                SkinCompatUserThemeManager.get().apply();
            }
        });
    }

    public static void setSwitchColor(SwitchCompat v) {

        // thumb color
        int thumbColor = 0xff000000;

        // trackColor
        int trackColor = 0xff000000;

        // set the thumb color
        DrawableCompat.setTintList(v.getThumbDrawable(), new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{}
                },
                new int[]{
                        thumbColor,
                        trackColor
                }));

        // set the track color
        DrawableCompat.setTintList(v.getTrackDrawable(), new ColorStateList(
                new int[][]{
                        new int[]{android.R.attr.state_checked},
                        new int[]{}
                },
                new int[]{
                        0x4DFF6633,
                        0x4d2f2f2f
                }));
    }

}
