package com.xfhy.skin;

import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import skin.support.content.res.SkinCompatUserThemeManager;

public class MainActivity extends AppCompatActivity {

    boolean isCLYH = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //初始化
        findViewById(R.id.btn_init).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SkinCompatUserThemeManager.get().addColorState(R.color.colorPrimary, "#40FF6D");
                SkinCompatUserThemeManager.get().addColorState(R.color.colorAccent, "#40FF6D");
                SkinCompatUserThemeManager.get().addColorState(R.color.colorPrimaryDark, "#40FF6D");
                SkinCompatUserThemeManager.get().addColorState(R.color.normal, "#FF0000");
                SkinCompatUserThemeManager.get().addColorState(R.color.pressed, "#ffffff");
                SkinCompatUserThemeManager.get().addColorState(R.color.text_color_normal, "#ffffff");
                SkinCompatUserThemeManager.get().addColorState(R.color.text_color_pressed, "#ff0000");
                SkinCompatUserThemeManager.get().apply();

                //准备PopupWindow的布局View
                View popupView = LayoutInflater.from(MainActivity.this).inflate(R.layout.popup, null);
                //初始化一个PopupWindow，width和height都是WRAP_CONTENT
                PopupWindow popupWindow = new PopupWindow(
                        ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                //设置PopupWindow的视图内容
                popupWindow.setContentView(popupView);
                //点击空白区域PopupWindow消失，这里必须先设置setBackgroundDrawable，否则点击无反应
                popupWindow.setBackgroundDrawable(new ColorDrawable(0xFFFFFFFF));
                popupWindow.setOutsideTouchable(true);
                //设置是否允许PopupWindow的范围超过屏幕范围
                popupWindow.setClippingEnabled(true);
                //设置PopupWindow消失监听
                popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
                    @Override
                    public void onDismiss() {

                    }
                });
                //PopupWindow在targetView下方弹出
                popupWindow.showAsDropDown(v);

            }
        });

        //切换颜色
        findViewById(R.id.btn_test).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isCLYH = !isCLYH;
                if (isCLYH) {
                    SkinCompatUserThemeManager.get().addColorState(R.color.colorPrimary, "#3F51B5");
                    SkinCompatUserThemeManager.get().addColorState(R.color.colorAccent, "#FF4081");
                    SkinCompatUserThemeManager.get().addColorState(R.color.colorPrimaryDark, "#303F9F");

                    SkinCompatUserThemeManager.get().addColorState(R.color.normal, "#decf25");
                    SkinCompatUserThemeManager.get().addColorState(R.color.pressed, "#3fcba6");

                    SkinCompatUserThemeManager.get().addColorState(R.color.text_color_normal, "#8bde25");
                    SkinCompatUserThemeManager.get().addColorState(R.color.text_color_pressed, "#d00cef");

                    SkinCompatUserThemeManager.get().addColorState(R.color.title_checked_text_color,
                            "#3F51B5");
                    SkinCompatUserThemeManager.get().addColorState(R.color.title_unchecked_text_color,
                            "#F0F0F0");
                    SkinCompatUserThemeManager.get().addColorState(R.color.title_checked_background_color,
                            "#FFFFFF");
                    SkinCompatUserThemeManager.get().addColorState(R.color
                            .title_unchecked_background_color, "#3F51B5");
                } else {
                    SkinCompatUserThemeManager.get().addColorState(R.color.colorPrimary, "#40FF6D");
                    SkinCompatUserThemeManager.get().addColorState(R.color.colorAccent, "#40FF6D");
                    SkinCompatUserThemeManager.get().addColorState(R.color.colorPrimaryDark, "#40FF6D");
                    SkinCompatUserThemeManager.get().addColorState(R.color.normal, "#FF0000");
                    SkinCompatUserThemeManager.get().addColorState(R.color.pressed, "#ffffff");
                    SkinCompatUserThemeManager.get().addColorState(R.color.text_color_normal, "#ffffff");
                    SkinCompatUserThemeManager.get().addColorState(R.color.text_color_pressed, "#ff0000");
                    SkinCompatUserThemeManager.get().addColorState(R.color.title_checked_text_color,
                            "#FF0000");

                    SkinCompatUserThemeManager.get().addColorState(R.color.title_checked_text_color,
                            "#decf25");
                    SkinCompatUserThemeManager.get().addColorState(R.color.title_unchecked_text_color,
                            "#3fcba6");
                    SkinCompatUserThemeManager.get().addColorState(R.color.title_checked_background_color,
                            "#8bde25");
                    SkinCompatUserThemeManager.get().addColorState(R.color
                            .title_unchecked_background_color, "#d00cef");
                }
                SkinCompatUserThemeManager.get().apply();
            }
        });
    }
}
