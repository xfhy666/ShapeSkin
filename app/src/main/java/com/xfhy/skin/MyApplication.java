package com.xfhy.skin;

import android.app.Application;
import android.support.v7.app.AppCompatDelegate;

import skin.support.SkinCompatManager;
import skin.support.app.SkinAppCompatViewInflater;
import skin.support.app.SkinCardViewInflater;
import skin.support.constraint.app.SkinConstraintViewInflater;
import skin.support.content.res.SkinCompatUserThemeManager;
import skin.support.design.app.SkinMaterialViewInflater;
import skin.support.utils.Slog;

/**
 * Created by xfhy on 2018/6/27 15:36
 * Description :
 */
public class MyApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();
        Slog.DEBUG = true;
        /*SkinCompatManager.withoutActivity(this)
                .setSkinStatusBarColorEnable(true)    //打开状态栏换肤
//                .addStrategy(CustomSDCardLoader())
//                .setSkinWindowBackgroundEnable(true)
                .setSkinAllActivityEnable(true)  //true: 默认所有的Activity都换肤;
                .loadSkin();*/


        SkinCompatManager.withoutActivity(this)
                .addInflater(new SkinAppCompatViewInflater())   // 基础控件换肤
                .addInflater(new SkinMaterialViewInflater())    // material design
                .addInflater(new SkinConstraintViewInflater())  // ConstraintLayout
                .addInflater(new SkinCardViewInflater())        // CardView v7
                .setSkinStatusBarColorEnable(true)    //打开状态栏换肤
                .setSkinAllActivityEnable(true)  //true: 默认所有的Activity都换肤;
                .setSkinStatusBarColorEnable(true);              // 关闭状态栏换肤

        //SVG兼容
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);

    }
}
