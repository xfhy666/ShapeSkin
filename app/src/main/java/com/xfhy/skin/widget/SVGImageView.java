package com.xfhy.skin.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.util.AttributeSet;

import com.xfhy.skin.R;

import skin.support.content.res.ColorState;
import skin.support.content.res.SkinCompatUserThemeManager;
import skin.support.widget.SkinCompatImageView;

/**
 * Created by xfhy on 2018/7/6 8:18
 * Description : 支持SVG换肤的ImageView
 */
public class SVGImageView extends SkinCompatImageView {
    public SVGImageView(Context context) {
        this(context, null);
    }

    public SVGImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SVGImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        skinChange();
    }

    private void skinChange() {
        Drawable drawable = getDrawable();
        ColorState colorState = SkinCompatUserThemeManager.get().getColorState(R.color.colorAccent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //API 21以上的处理方式
            if (drawable instanceof VectorDrawable) {
                VectorDrawable vectorDrawable = (VectorDrawable) drawable;
                if (colorState == null) {
                    vectorDrawable.setTint(getResources().getColor(R.color.colorAccent));
                } else {
                    vectorDrawable.setTint(Color.parseColor(colorState.getColorDefault()));
                }
            }
        } else {
            //兼容API21 以下的手机
            VectorDrawableCompat vectorDrawableCompat = VectorDrawableCompat.create(getResources(), R.drawable
                    .ic_beach_access_black, getContext().getTheme());
            if (vectorDrawableCompat != null) {
                if (colorState == null) {
                    vectorDrawableCompat.setTint(getResources().getColor(R.color.colorAccent));
                } else {
                    vectorDrawableCompat.setTint(Color.parseColor(colorState.getColorDefault()));
                }
                setImageDrawable(vectorDrawableCompat);
            }
        }

    }

    @Override
    public void applySkin() {
        super.applySkin();
        skinChange();
    }
}
