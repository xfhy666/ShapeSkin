package com.xfhy.skin.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;

import com.xfhy.skin.R;

import skin.support.content.res.ColorState;
import skin.support.content.res.SkinCompatUserThemeManager;
import skin.support.widget.SkinCompatButton;

/**
 * Created by xfhy on 2018/6/27 8:10
 * Description : 支持shape换肤,支持Selector
 */
public class SelectorButton extends SkinCompatButton {

    /**
     * 是否开启selector模式
     */
    private boolean mIsOpenSelecMode = false;

    public SelectorButton(Context context) {
        super(context);
    }

    public SelectorButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.buttonStyle);
    }

    public SelectorButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SelectorButton);
        mIsOpenSelecMode = typedArray.getBoolean(R.styleable.SelectorButton_selectorOpen, false);
        typedArray.recycle();
        initDrawable();
    }

    private void initDrawable() {
        Drawable drawable = getBackground();
        if (drawable instanceof GradientDrawable) {
            Log.e("xfhy", "initDrawable");
            GradientDrawable gradientDrawable = (GradientDrawable) drawable;
            ColorState colorState = SkinCompatUserThemeManager.get().getColorState("normal");
            if (colorState == null) {
                gradientDrawable.setColor(getResources().getColor(R.color.normal));
            } else {
                int parseColor = Color.parseColor(colorState.getColorDefault());
                gradientDrawable.setColor(parseColor);
            }
        }
    }

    @Override
    public void applySkin() {
        super.applySkin();
        initDrawable();

        //不需要selector
        if (!mIsOpenSelecMode) {
            return;
        }

        ColorState colorStateNormal = SkinCompatUserThemeManager.get().getColorState("normal");
        Drawable background = getBackground();
        //三方库  无缓存的颜色
        if (background instanceof GradientDrawable) {
            GradientDrawable gradientDrawable = (GradientDrawable) background;
            if (colorStateNormal == null) {
                gradientDrawable.setColor(getResources().getColor(R.color.normal));
            } else {
                gradientDrawable.setColor(Color.parseColor(colorStateNormal.getColorDefault()));
            }
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //如果未开启selector模式,则直接返回
        if (!mIsOpenSelecMode) {
            return super.onTouchEvent(event);
        }
        Drawable drawable = getBackground();

        if (!(drawable instanceof GradientDrawable)) {
            return super.onTouchEvent(event);
        }
        GradientDrawable gradientDrawable = (GradientDrawable) drawable;

        ColorState colorStateNormal = SkinCompatUserThemeManager.get().getColorState("normal");
        ColorState colorStatePressed = SkinCompatUserThemeManager.get().getColorState("pressed");

        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (colorStatePressed == null) {
                    gradientDrawable.setColor(getResources().getColor(R.color.pressed));
                } else {
                    gradientDrawable.setColor(Color.parseColor(colorStatePressed.getColorDefault()));
                }
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                if (colorStateNormal == null) {
                    gradientDrawable.setColor(getResources().getColor(R.color.normal));
                } else {
                    gradientDrawable.setColor(Color.parseColor(colorStateNormal.getColorDefault()));
                }
                break;
        }
        return super.onTouchEvent(event);
    }

}
