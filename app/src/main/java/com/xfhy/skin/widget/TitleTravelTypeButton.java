package com.xfhy.skin.widget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.RadioGroup;

import com.xfhy.skin.R;

import skin.support.content.res.ColorState;
import skin.support.content.res.SkinCompatUserThemeManager;
import skin.support.widget.SkinCompatRadioButton;
import skin.support.widget.SkinCompatRadioGroup;

/**
 * Created by xfhy on 2018/7/3 18:46
 * Description : 自定义顶部标题栏的因公因私按钮   从布局加载
 */
public class TitleTravelTypeButton extends SkinCompatRadioGroup implements RadioGroup
        .OnCheckedChangeListener {

    private OnCheckChangedListener mListener;
    private SkinCompatRadioButton mLeftBtn;
    private SkinCompatRadioButton mRightBtn;
    private RadioGroup mRootView;

    public TitleTravelTypeButton(Context context) {
        this(context, null);
    }

    public TitleTravelTypeButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        initView();
        initDrawable();
    }

    private void initView() {
        View.inflate(getContext(), R.layout.layout_title_travel_type, this);

        mRootView = findViewById(R.id.rg_root_title);
        mLeftBtn = findViewById(R.id.btn_public_title);
        mRightBtn = findViewById(R.id.btn_private_title);
        mRootView.setOnCheckedChangeListener(this);
    }

    private void initDrawable() {
        Drawable leftDrawable = mLeftBtn.getBackground();
        Drawable rightDrawable = mRightBtn.getBackground();
        Drawable rootViewBackground = mRootView.getBackground();
        ColorState checkedBgColorState = SkinCompatUserThemeManager.get().getColorState(R.color
                .title_checked_background_color);
        ColorState unCheckedBgColorState = SkinCompatUserThemeManager.get().getColorState(R.color
                .title_unchecked_background_color);
        ColorState checkedTextColorState = SkinCompatUserThemeManager.get().getColorState(R.color
                .title_checked_text_color);
        ColorState unCheckedTextColorState = SkinCompatUserThemeManager.get().getColorState(R.color
                .title_unchecked_text_color);

        if (rootViewBackground instanceof GradientDrawable) {
            GradientDrawable gradientDrawable = (GradientDrawable) rootViewBackground;
            if (unCheckedBgColorState == null) {
                gradientDrawable.setColor(getResources().getColor(R.color
                        .title_unchecked_background_color));
            } else {
                gradientDrawable.setColor(Color.parseColor(unCheckedBgColorState.getColorDefault()));
            }
        }

        if (mLeftBtn.isChecked()) { //选中的是左边按钮
            if (leftDrawable instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) leftDrawable;
                if (checkedBgColorState == null) {
                    gradientDrawable.setColor(getResources().getColor(R.color
                            .title_checked_background_color));
                } else {
                    gradientDrawable.setColor(Color.parseColor(checkedBgColorState.getColorDefault()));
                }
            }
            if (rightDrawable instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) rightDrawable;
                if (unCheckedBgColorState == null) {
                    gradientDrawable.setColor(getResources().getColor(R.color
                            .title_unchecked_background_color));
                } else {
                    gradientDrawable.setColor(Color.parseColor(unCheckedBgColorState.getColorDefault()));
                }
            }

            //文字颜色
            if (checkedTextColorState == null) {
                mLeftBtn.setTextColor(getResources().getColor(R.color.title_checked_text_color));
            } else {
                mLeftBtn.setTextColor(Color.parseColor(checkedTextColorState.getColorDefault()));
            }
            if (unCheckedTextColorState == null) {
                mRightBtn.setTextColor(getResources().getColor(R.color.title_unchecked_text_color));
            } else {
                mRightBtn.setTextColor(Color.parseColor(unCheckedTextColorState.getColorDefault()));
            }
        } else {
            if (leftDrawable instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) leftDrawable;
                if (unCheckedBgColorState == null) {
                    gradientDrawable.setColor(getResources().getColor(R.color
                            .title_unchecked_background_color));
                } else {
                    gradientDrawable.setColor(Color.parseColor(unCheckedBgColorState.getColorDefault()));
                }
            }
            if (rightDrawable instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) rightDrawable;
                if (checkedBgColorState == null) {
                    gradientDrawable.setColor(getResources().getColor(R.color
                            .title_checked_background_color));
                } else {
                    gradientDrawable.setColor(Color.parseColor(checkedBgColorState.getColorDefault()));
                }
            }

            //文字颜色
            if (unCheckedTextColorState == null) {
                mLeftBtn.setTextColor(getResources().getColor(R.color.title_unchecked_text_color));
            } else {
                mLeftBtn.setTextColor(Color.parseColor(unCheckedTextColorState.getColorDefault()));
            }
            if (checkedTextColorState == null) {
                mRightBtn.setTextColor(getResources().getColor(R.color.title_checked_text_color));
            } else {
                mRightBtn.setTextColor(Color.parseColor(checkedTextColorState.getColorDefault()));
            }
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        switch (checkedId) {
            case R.id.btn_public_title:
                Log.e("xfhy", "左边按钮");
                leftBtnClick();
                break;
            case R.id.btn_private_title:
                Log.e("xfhy", "右边按钮");
                rightBtnClick();
                break;
            default:
                break;
        }
        initDrawable();
    }

    private void leftBtnClick() {
        if (mListener != null) {
            mListener.checkChanged(true);
        }
    }

    private void rightBtnClick() {
        if (mListener != null) {
            mListener.checkChanged(false);
        }
    }

    @Override
    public void applySkin() {
        super.applySkin();
        initDrawable();
    }

    public void setOnCheckChangedListener(OnCheckChangedListener listener) {
        mListener = listener;
    }

    public interface OnCheckChangedListener {
        /**
         * 选中状态改变
         *
         * @param isLeftChecked 左边是否选中
         */
        void checkChanged(boolean isLeftChecked);
    }

}
