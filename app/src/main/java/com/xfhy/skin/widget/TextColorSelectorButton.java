package com.xfhy.skin.widget;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.xfhy.skin.R;

import skin.support.content.res.ColorState;
import skin.support.content.res.SkinCompatUserThemeManager;
import skin.support.widget.SkinCompatButton;

/**
 * Created by xfhy on 2018/6/29 8:23
 * Description : 可以动态切换color颜色是selector类型的
 */
public class TextColorSelectorButton extends SkinCompatButton {

    //private ColorStateList mColorStateList;
    /*private int mColorForStatePressed;
    private int mColorForStateNormal;*/

    public TextColorSelectorButton(Context context) {
        this(context, null);
    }

    public TextColorSelectorButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.buttonStyle);
    }

    public TextColorSelectorButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //最开始的颜色值
        //mColorStateList = getTextColors();
       /* mColorForStatePressed = mColorStateList.getColorForState(new int[]{android.R.attr.state_pressed}, R
                .color.text_color_pressed);
        mColorForStateNormal = mColorStateList.getColorForState(new int[]{android.R.attr.state_enabled}, R
                .color.text_color_normal);*/
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        ColorState textColorStateNormal = SkinCompatUserThemeManager.get().getColorState(R.color
                .text_color_normal);
        ColorState textColorStatePressed = SkinCompatUserThemeManager.get().getColorState(R.color
                .text_color_pressed);

        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (textColorStatePressed == null) {
                    setTextColor(getResources().getColor(R.color.text_color_pressed));
                } else {
                    setTextColor(Color.parseColor(textColorStatePressed.getColorDefault()));
                }
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                if (textColorStateNormal == null) {
                    setTextColor(getResources().getColor(R.color.text_color_normal));
                } else {
                    setTextColor(Color.parseColor(textColorStateNormal.getColorDefault()));
                }
                break;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void applySkin() {
        super.applySkin();
        ColorState textColorStateNormal = SkinCompatUserThemeManager.get().getColorState(R.color
                .text_color_normal);
        if (textColorStateNormal == null) {
            setTextColor(getResources().getColor(R.color.text_color_normal));
        } else {
            setTextColor(Color.parseColor(textColorStateNormal.getColorDefault()));
        }
    }
}
