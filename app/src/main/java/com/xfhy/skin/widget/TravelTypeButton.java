package com.xfhy.skin.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import com.xfhy.skin.DensityUtil;
import com.xfhy.skin.R;

import skin.support.widget.SkinCompatView;

/**
 * Created by xfhy on 2018/7/3 11:38
 * Description : 因公因私 按钮
 */
@Deprecated
public class TravelTypeButton extends SkinCompatView {

    //选中背景色
    //未选中背景色
    //外圈边框

    private final static int TEXT_SIZE_DEFAULT = 18;
    private int mWidth;
    private int mHeight;
    private boolean mIsLeftChecked = true;
    private OnCheckChangedListener mListener;
    private Paint mCheckedPaint;
    private Paint mUnCheckedPaint;
    private Paint mCheckedTextPaint;
    private Paint mUnCheckedTextPaint;

    public TravelTypeButton(Context context) {
        this(context, null);
    }

    public TravelTypeButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TravelTypeButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initView();
    }

    private void initView() {
        //背景
        mCheckedPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCheckedPaint.setColor(getResources().getColor(R.color.pressed));
        mUnCheckedPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mUnCheckedPaint.setColor(getResources().getColor(R.color.normal));

        //文字
        int textSize = DensityUtil.sp2px(getContext(), TEXT_SIZE_DEFAULT);
        mCheckedTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mCheckedTextPaint.setColor(getResources().getColor(R.color.text_color_normal));
        mCheckedTextPaint.setTextSize(textSize);
        mUnCheckedTextPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mUnCheckedTextPaint.setColor(getResources().getColor(R.color.text_color_pressed));
        mUnCheckedTextPaint.setTextSize(textSize);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        drawBackground(canvas);
        drawText(canvas);
    }

    /**
     * 画文字
     */
    private void drawText(Canvas canvas) {
        //Paint.FontMetrics checkedFontMetrics = mCheckedTextPaint.getFontMetrics();
        //文字高度
        Rect bounds = new Rect();
        mCheckedTextPaint.getTextBounds("因 公", 0, "因 公".length(), bounds);
        float textHeight = bounds.bottom + Math.abs(bounds.top);
        float textY = mHeight / 2 + textHeight / 2;

        //因公宽度
        float businessTextWidth = mCheckedTextPaint.measureText("因 公");
        //因私宽度
        float privateTextWidth = mCheckedTextPaint.measureText("因 私");
        if (mIsLeftChecked) {  //选中左边
            //画左边文字
            //左边文字左下角坐标:(mWidth/4-textWidth/2,mHeight/2+textHeight/2)
            canvas.drawText("因 公", mWidth / 4 - businessTextWidth / 2, textY,
                    mCheckedTextPaint);
            //画右边文字
            //右边文字左下角坐标:(mWidth/4*3-textWidth/2,mHeight/2+textHeight/2)
            canvas.drawText("因 私", mWidth / 4 * 3 - privateTextWidth / 2, textY,
                    mUnCheckedTextPaint);
        } else {
            canvas.drawText("因 公", mWidth / 4 - businessTextWidth / 2, textY,
                    mUnCheckedTextPaint);
            canvas.drawText("因 私", mWidth / 4 * 3 - privateTextWidth / 2, textY,
                    mCheckedTextPaint);
        }
    }

    /**
     * 画背景
     */
    private void drawBackground(Canvas canvas) {
        Rect checkedRect;
        Rect unCheckedRect;
        if (mIsLeftChecked) {
            checkedRect = new Rect(0, 0, mWidth / 2, mHeight);
            unCheckedRect = new Rect(mWidth / 2, 0, mWidth, mHeight);
        } else {
            checkedRect = new Rect(mWidth / 2, 0, mWidth, mHeight);
            unCheckedRect = new Rect(0, 0, mWidth / 2, mHeight);
        }
        canvas.drawRect(checkedRect, mCheckedPaint);
        canvas.drawRect(unCheckedRect, mUnCheckedPaint);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                analyClickArea(event);
                changeCheckedArea();
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                break;
            default:
                break;
        }
        return super.onTouchEvent(event);
    }

    /**
     * 选中区域变化,及时更新UI
     */
    private void changeCheckedArea() {
        //画左边  或者画右边
        invalidate();
    }

    private void analyClickArea(MotionEvent event) {
        float clickX = event.getX();
        int halfWidth = mWidth / 2;
        if (clickX < halfWidth) { //点击左边
            if (!mIsLeftChecked && mListener != null) {
                //如果之前就是选中的左边  则不回调   ,之前不是选择的左边才回调
                mListener.checkChanged(true);
                Log.e("xfhy", "左边被点击");
            }
            mIsLeftChecked = true;
        } else { //点击右边
            if (mIsLeftChecked && mListener != null) {
                //如果之前就是选中的右边  则不回调   ,之前不是选择的右边才回调
                mListener.checkChanged(false);
                Log.e("xfhy", "右边被点击");
            }
            mIsLeftChecked = false;
        }
    }

    public void setOnCheckChangedListener(OnCheckChangedListener listener) {
        mListener = listener;
    }

    public interface OnCheckChangedListener {
        /**
         * 选中状态改变
         *
         * @param isLeftChecked 左边是否选中
         */
        void checkChanged(boolean isLeftChecked);
    }

}
