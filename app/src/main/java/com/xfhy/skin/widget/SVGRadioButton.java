package com.xfhy.skin.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.content.res.AppCompatResources;
import android.util.AttributeSet;

import com.xfhy.skin.R;

import skin.support.content.res.ColorState;
import skin.support.content.res.SkinCompatUserThemeManager;
import skin.support.widget.SkinCompatRadioButton;

/**
 * Created by xfhy on 2018/7/14 9:52
 * Description :
 * <p>
 * android:drawableTop="@drawable/icon_svg"  这样直接设置在5.0以下会崩溃,需要用selector进行包裹:icon_svg_selector
 * <p>
 * 参考:https://blog.csdn.net/llxy21/article/details/53303113
 * https://blog.csdn.net/yuzhiqiang_1993/article/details/78032465
 */
public class SVGRadioButton extends SkinCompatRadioButton {

    private int mResourceTopId;

    private Drawable mTopDrawable;

    public SVGRadioButton(Context context) {
        this(context, null);
    }

    public SVGRadioButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SVGRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SVGRadioButton);
        Drawable dl = null;
        Drawable dt = null;
        Drawable dr = null;
        Drawable db = null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mTopDrawable = typedArray.getDrawable(R.styleable.SVGRadioButton_drawableCompatTop);
        } else {
            int dtId = typedArray.getResourceId(R.styleable.SVGRadioButton_drawableCompatTop, -1);

            if (dtId != -1) {
                mTopDrawable = AppCompatResources.getDrawable(context, dtId);
            }
        }
        typedArray.recycle();

        skinChange();
    }

    private void skinChange() {
        if (mResourceTopId != -1) {
            mTopDrawable = setDrawableColor(mTopDrawable);
        }

        setCompoundDrawablesRelativeWithIntrinsicBounds(null, mTopDrawable, null, null);     // 4.4.4 可用
    }

    private Drawable setDrawableColor(Drawable drawable) {
        ColorState colorState = SkinCompatUserThemeManager.get().getColorState(R.color.colorAccent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (drawable instanceof VectorDrawable) {
                VectorDrawable vectorDrawable = (VectorDrawable) drawable;
                if (colorState == null) {
                    vectorDrawable.setTint(getResources().getColor(R.color.colorAccent));
                } else {
                    vectorDrawable.setTint(Color.parseColor(colorState.getColorDefault()));
                }
                return vectorDrawable;
            }
        } else {
            if (drawable instanceof VectorDrawableCompat) {
                VectorDrawableCompat vectorDrawableCompat = (VectorDrawableCompat) drawable;
                if (colorState == null) {
                    vectorDrawableCompat.setTint(getResources().getColor(R.color.colorAccent));
                } else {
                    vectorDrawableCompat.setTint(Color.parseColor(colorState.getColorDefault()));
                }
                return vectorDrawableCompat;
            }
        }
        return null;
    }

    @Override
    public void applySkin() {
        super.applySkin();
        skinChange();
    }
}
