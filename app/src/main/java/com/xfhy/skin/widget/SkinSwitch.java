package com.xfhy.skin.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.xfhy.skin.R;

import skin.support.widget.SkinCompatView;

/**
 * Created by xfhy on 2018/7/25 17:28
 * Description : 支持换肤的开关控件
 */
public class SkinSwitch extends SkinCompatView {

    private static final int BORDER_WIDTH = 5;
    private static final int DEFAULT_PADDING = 10;

    private Paint mWhitePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint mBackGroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint mBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint mLeftOvalBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private boolean mIsChecked = false;
    private int mWidth;
    private int mHeight;
    private Path mOuterGrayPath;

    public SkinSwitch(Context context) {
        this(context, null);
    }

    public SkinSwitch(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.checkboxStyle);
    }

    public SkinSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        initView();
    }

    private void initView() {
        mWhitePaint.setColor(Color.WHITE);
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setStrokeWidth(BORDER_WIDTH);
        mBorderPaint.setColor(Color.parseColor("#cdcdcd"));
        mLeftOvalBorderPaint.setStyle(Paint.Style.STROKE);
        mLeftOvalBorderPaint.setStrokeWidth(BORDER_WIDTH);
        mLeftOvalBorderPaint.setColor(Color.parseColor("#cdcdcd"));
        mLeftOvalBorderPaint.setShadowLayer(5, 0, 3, Color.parseColor("#cdcdcd"));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;

        //提前计算好未选中时的边框  避免在onDraw()中耗时
        mOuterGrayPath = new Path();
        RectF leftOval = new RectF(DEFAULT_PADDING, DEFAULT_PADDING, mHeight - DEFAULT_PADDING, mHeight - DEFAULT_PADDING);
        mOuterGrayPath.addArc(leftOval, -90, -180);
        mOuterGrayPath.moveTo(mHeight / 2, DEFAULT_PADDING);
        mOuterGrayPath.lineTo(mWidth - mHeight / 2, DEFAULT_PADDING);
        RectF rightHalfOval = new RectF(mWidth - mHeight, DEFAULT_PADDING, mWidth - DEFAULT_PADDING, mHeight - DEFAULT_PADDING);
        mOuterGrayPath.addArc(rightHalfOval, -90, 180);
        mOuterGrayPath.lineTo(mHeight / 2, mHeight - DEFAULT_PADDING);
    }

    @SuppressLint("DrawAllocation")
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mIsChecked) {
            //画主题背景和白色圆   其实就是一根线条和一个白色的圆
            mBackGroundPaint.setColor(Color.parseColor("#46b5f5"));
            mBackGroundPaint.setStrokeWidth(mHeight - 2 * DEFAULT_PADDING);
            mBackGroundPaint.setStrokeCap(Paint.Cap.ROUND);
            //线条的起点圆半径
            int radius = (mHeight - 2 * DEFAULT_PADDING) / 2;
            canvas.drawLine(DEFAULT_PADDING + radius, mHeight / 2, mWidth - DEFAULT_PADDING - radius, mHeight / 2, mBackGroundPaint);

            //白色圆 稍微离边界有点距离
            canvas.drawCircle(mWidth - DEFAULT_PADDING - radius, mHeight / 2, radius - 3, mWhitePaint);
        } else {
            //画灰色边框
            canvas.drawPath(mOuterGrayPath, mBorderPaint);

            canvas.drawCircle(mHeight / 2, mHeight / 2,
                    mHeight / 2 - DEFAULT_PADDING - mBorderPaint.getStrokeWidth() / 2, mLeftOvalBorderPaint);
            /*RectF rectF = new RectF(DEFAULT_PADDING + mBorderPaint.getStrokeWidth() / 2,
                    DEFAULT_PADDING + mBorderPaint.getStrokeWidth() / 2,
                    DEFAULT_PADDING + mBorderPaint.getStrokeWidth() + mHeight - 2 * DEFAULT_PADDING - 2 * mBorderPaint.getStrokeWidth() / 2,
                    mHeight - DEFAULT_PADDING - mBorderPaint.getStrokeWidth() / 2);
            canvas.drawArc(rectF, 90, 270, false, mLeftOvalBorderPaint);
            canvas.drawArc(rectF, 90, -90, false, mLeftOvalBorderPaint);*/
        }
    }

    @Override
    public void applySkin() {
        super.applySkin();
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mIsChecked = !mIsChecked;
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                break;
            default:
                break;
        }
        return super.onTouchEvent(event);
    }

    public boolean isChecked() {
        return mIsChecked;
    }

    public void setChecked(boolean checked) {
        mIsChecked = checked;
        invalidate();
    }
}
