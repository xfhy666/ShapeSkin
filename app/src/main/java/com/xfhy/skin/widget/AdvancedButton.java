package com.xfhy.skin.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;

import com.xfhy.skin.R;

import skin.support.content.res.ColorState;
import skin.support.content.res.SkinCompatUserThemeManager;
import skin.support.widget.SkinCompatButton;

/**
 * Created by xfhy on 2018/6/29 14:21
 * Description : 高级按钮  支持textColor的selector   和背景的selector
 */
public class AdvancedButton extends SkinCompatButton {

    public AdvancedButton(Context context) {
        this(context, null);
    }

    public AdvancedButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.buttonStyle);
    }

    public AdvancedButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initDrawable();
    }

    private void initDrawable() {
        Drawable drawable = getBackground();
        if (drawable instanceof GradientDrawable) {
            Log.e("xfhy", "initDrawable");
            GradientDrawable gradientDrawable = (GradientDrawable) drawable;
            ColorState colorState = SkinCompatUserThemeManager.get().getColorState("normal");
            if (colorState == null) {
                gradientDrawable.setColor(getResources().getColor(R.color.normal));
            } else {
                int parseColor = Color.parseColor(colorState.getColorDefault());
                gradientDrawable.setColor(parseColor);
            }
        }
    }

    @Override
    public void applySkin() {
        super.applySkin();
        initDrawable();

        //背景颜色
        ColorState colorStateNormal = SkinCompatUserThemeManager.get().getColorState("normal");
        Drawable background = getBackground();
        //三方库  需要判断有无缓存的颜色
        if (background instanceof GradientDrawable) {
            GradientDrawable gradientDrawable = (GradientDrawable) background;
            if (colorStateNormal == null) {
                gradientDrawable.setColor(getResources().getColor(R.color.normal));
            } else {
                gradientDrawable.setColor(Color.parseColor(colorStateNormal.getColorDefault()));
            }
        }

        //文字颜色
        ColorState textColorStateNormal = SkinCompatUserThemeManager.get().getColorState(R.color
                .text_color_normal);
        if (textColorStateNormal == null) {
            setTextColor(getResources().getColor(R.color.text_color_normal));
        } else {
            setTextColor(Color.parseColor(textColorStateNormal.getColorDefault()));
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        Drawable drawable = getBackground();

        if (!(drawable instanceof GradientDrawable)) {
            return super.onTouchEvent(event);
        }
        GradientDrawable gradientDrawable = (GradientDrawable) drawable;

        ColorState colorStateNormal = SkinCompatUserThemeManager.get().getColorState("normal");
        ColorState colorStatePressed = SkinCompatUserThemeManager.get().getColorState("pressed");

        //文字颜色
        ColorState textColorStateNormal = SkinCompatUserThemeManager.get().getColorState(R.color
                .text_color_normal);
        ColorState textColorStatePressed = SkinCompatUserThemeManager.get().getColorState(R.color
                .text_color_pressed);

        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                if (colorStatePressed == null) {
                    gradientDrawable.setColor(getResources().getColor(R.color.pressed));
                } else {
                    gradientDrawable.setColor(Color.parseColor(colorStatePressed.getColorDefault()));
                }
                if (textColorStatePressed == null) {
                    setTextColor(getResources().getColor(R.color.text_color_pressed));
                } else {
                    setTextColor(Color.parseColor(textColorStatePressed.getColorDefault()));
                }
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                if (colorStateNormal == null) {
                    gradientDrawable.setColor(getResources().getColor(R.color.normal));
                } else {
                    gradientDrawable.setColor(Color.parseColor(colorStateNormal.getColorDefault()));
                }
                if (textColorStateNormal == null) {
                    setTextColor(getResources().getColor(R.color.text_color_normal));
                } else {
                    setTextColor(Color.parseColor(textColorStateNormal.getColorDefault()));
                }
                break;
        }
        return super.onTouchEvent(event);
    }

}
