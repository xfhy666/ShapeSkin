package com.xfhy.skin.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.annotation.ColorRes;
import android.util.AttributeSet;
import android.util.TypedValue;

import com.tools.R;

import support.SkinManager;
import support.widget.SkinCompatTextView;
import support.widget.model.SkinFont;

import static support.widget.SkinCompatHelper.INVALID_ID;

/**
 * Created by xfhy on 2018/7/10 8:05
 * Description : 换肤字体的TextView
 */
public class SkinFontTextView extends SkinCompatTextView {

    /**
     * 标题栏字体
     * <enum name="title" value="1000"
     * 主要内容常规字体
     * <enum name="main_content_routine" value="1001"
     * 次要内容字体
     * <enum name="secondary_content" value="1002"
     * 备注性说明内容字体
     * <enum name="remarks" value="1003"
     * 链接字体
     * <enum name="link" value="1004"
     * 次要链接字体：
     * <enum name="secondary_link" value="1005"
     * 主要按钮字体：
     * <enum name="main_button" value="1006"
     * 次要按钮字体
     * <enum name="secondary_button" value="1007"
     * 预订字体
     * <enum name="booking_font" value="1008"
     * 普通提示字体
     * <enum name="ordinary_prompt" value="1009"
     * 重要提示字体
     * <enum name="important_hint" value="1010"
     * 警告提示字体
     * <enum name="warning" value="1011"
     * 金额字体
     * <enum name="amount" value="1012"
     */
    public static final int DEFAULT_FONT_SIZE_12 = 12;
    public static final int DEFAULT_FONT_SIZE_14 = 14;
    public static final int DEFAULT_FONT_SIZE_17 = 17;
    public static final int NO_DEFAULT_FONT_SIZE = -1;

    private int mType = INVALID_ID;

    public SkinFontTextView(Context context) {
        this(context, null);
    }

    public SkinFontTextView(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.textViewStyle);
    }

    public SkinFontTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SkinFontTextView);
        mType = typedArray.getInt(R.styleable.SkinFontTextView_SkinFontTextViewType, INVALID_ID);
        typedArray.recycle();
        skinChange();

    }

    protected void skinChange() {
        //从SkinManager中取数据
        SkinFont skinFont = null;
        switch (mType) {
            case SkinManager.TITLE_FONT:
                skinFont = SkinManager.getFontConfigByType(getContext(), SkinManager.TITLE_FONT);
                setFontType(skinFont, R.color.header_font, NO_DEFAULT_FONT_SIZE);
                break;
            case SkinManager.MAIN_CONTENT_ROUTINE:
                skinFont = SkinManager.getFontConfigByType(getContext(), SkinManager
                        .MAIN_CONTENT_ROUTINE);
                setFontType(skinFont, R.color.main_content_regular_font, DEFAULT_FONT_SIZE_14);
                break;
            case SkinManager.SECONDARY_CONTENT:
                skinFont = SkinManager.getFontConfigByType(getContext(), SkinManager
                        .SECONDARY_CONTENT);
                setFontType(skinFont, R.color.minor_content_font, DEFAULT_FONT_SIZE_14);
                break;
            case SkinManager.REMARKS:
                skinFont = SkinManager.getFontConfigByType(getContext(), SkinManager.REMARKS);
                setFontType(skinFont, R.color.note_the_content_font, DEFAULT_FONT_SIZE_12);
                break;
            case SkinManager.LINK:
                skinFont = SkinManager.getFontConfigByType(getContext(), SkinManager.LINK);
                setFontType(skinFont, R.color.link_the_font, DEFAULT_FONT_SIZE_14);
                break;
            case SkinManager.SECONDARY_LINK:
                skinFont = SkinManager.getFontConfigByType(getContext(), SkinManager
                        .SECONDARY_LINK);
                setFontType(skinFont, R.color.secondary_link_font, DEFAULT_FONT_SIZE_12);
                break;
            case SkinManager.MAIN_BUTTON:
                skinFont = SkinManager.getFontConfigByType(getContext(), SkinManager.MAIN_BUTTON);
                setFontType(skinFont, R.color.main_button_font, DEFAULT_FONT_SIZE_17);
                break;
            case SkinManager.SECONDARY_BUTTON:
                skinFont = SkinManager.getFontConfigByType(getContext(), SkinManager
                        .SECONDARY_BUTTON);
                setFontType(skinFont, R.color.secondary_button_font, DEFAULT_FONT_SIZE_17);
                break;
            case SkinManager.BOOKING_FONT:
                skinFont = SkinManager.getFontConfigByType(getContext(), SkinManager.BOOKING_FONT);
                setFontType(skinFont, R.color.booking_font, DEFAULT_FONT_SIZE_14);
                break;
            case SkinManager.ORDINARY_PROMPT:
                skinFont = SkinManager.getFontConfigByType(getContext(), SkinManager
                        .ORDINARY_PROMPT);
                setFontType(skinFont, R.color.general_prompt_font, NO_DEFAULT_FONT_SIZE);
                break;
            case SkinManager.IMPORTANT_HINT:
                skinFont = SkinManager.getFontConfigByType(getContext(), SkinManager
                        .IMPORTANT_HINT);
                setFontType(skinFont, R.color.important_prompt_font, DEFAULT_FONT_SIZE_14);
                break;
            case SkinManager.WARNING:
                skinFont = SkinManager.getFontConfigByType(getContext(), SkinManager.WARNING);
                setFontType(skinFont, R.color.warning_prompt_font, NO_DEFAULT_FONT_SIZE);
                break;
            case SkinManager.AMOUNT:
                skinFont = SkinManager.getFontConfigByType(getContext(), SkinManager.AMOUNT);
                setFontType(skinFont, R.color.the_amount_of_the_font, NO_DEFAULT_FONT_SIZE);
                break;
            case INVALID_ID:
                break;
            default:
                break;
        }
    }

    /**
     * 设置字体样式
     *
     * @param skinFont 配置数据
     * @param colorRes 默认颜色
     * @param fontSize 默认字体大小
     */
    private void setFontType(SkinFont skinFont, @ColorRes int colorRes, int fontSize) {
        if (skinFont == null) {
            //使用默认值
            setTextColor(getResources().getColor(colorRes));
            if (fontSize != NO_DEFAULT_FONT_SIZE) {  //某些View不需要更换字体大小
                setTextSize(TypedValue.COMPLEX_UNIT_DIP, fontSize);
            }
            setTypeface(Typeface.DEFAULT);
            return;
        }

        //皮肤配置
        setTextColor(skinFont.fontColor);
        if (fontSize != NO_DEFAULT_FONT_SIZE) {  //某些View不需要更换字体大小
            setTextSize(TypedValue.COMPLEX_UNIT_DIP, skinFont.fontSize / 2);
        }
        setTypeface(skinFont.thickness == 0 ? Typeface.DEFAULT_BOLD : Typeface.DEFAULT);
    }

    @Override
    public void applySkin() {
        super.applySkin();
        skinChange();
    }
}
