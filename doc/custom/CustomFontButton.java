package com.xfhy.skin.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.tools.R;

import support.SkinManager;

import static support.widget.SkinCompatHelper.INVALID_ID;

/**
 * Created by xfhy on 2018/7/12 8:44
 * Description : 特殊"按钮"(其实是TextView)  需要修改字体的用这个.同样是拥有背景selector效果
 * <p>
 * 单独写一个,免得和普通按钮混起了   避免改特殊按钮时影响了普通按钮
 */
public class CustomFontButton extends SkinFontTextView {

    /**
     * 本地默认按下颜色
     */
    private int mPressedBgColor;
    /**
     * 本地默认正常颜色
     */
    private int mNormalBgColor;
    /**
     * 按下颜色的资源id
     */
    private int mPressedBgColorResId;
    /**
     * 正常颜色的资源id
     */
    private int mNormalBgColorResId;

    public CustomFontButton(Context context) {
        super(context);
    }

    public CustomFontButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.buttonStyle);
    }

    public CustomFontButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        readCustomAttribute(context, attrs);
        skinChange();
    }

    private void readCustomAttribute(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomFontButton);
        mPressedBgColorResId = typedArray.getResourceId(R.styleable
                .CustomFontButton_pressedCustomFontButtonBGColor, INVALID_ID);
        mNormalBgColorResId = typedArray.getResourceId(R.styleable
                .CustomFontButton_normalCustomFontButtonBGColor, INVALID_ID);

        mPressedBgColor = typedArray.getColor(R.styleable.CustomFontButton_pressedCustomFontButtonBGColor,
                INVALID_ID);
        mNormalBgColor = typedArray.getColor(R.styleable.CustomFontButton_normalCustomFontButtonBGColor,
                INVALID_ID);
        if (mNormalBgColor == INVALID_ID) {
            mNormalBgColor = getSolidColor();
        }
        typedArray.recycle();
    }

    @Override
    protected void skinChange() {
        super.skinChange();

        //可用->修改背景
        if (isEnabled()) {
            Drawable drawable = getBackground();
            if (drawable instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                if (mNormalBgColor != INVALID_ID) {
                    gradientDrawable.setColor(SkinManager.getColorArgb(mNormalBgColor,
                            mNormalBgColorResId));
                }
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //5.0 以上才有
                if(drawable instanceof RippleDrawable){
                    setBackgroundColor(SkinManager.getColorArgb(mNormalBgColor, mNormalBgColorResId));
                }
            }
        }
    }

    @Override
    public void applySkin() {
        if (isEnabled()) {
            super.applySkin();
            skinChange();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        Drawable drawable = getBackground();

        //当前是不可用  或者   背景不是GradientDrawable
        if (!isEnabled() || !(drawable instanceof GradientDrawable)) {
            return super.onTouchEvent(event);
        }
        //用户没有设置按下时的颜色,则别去管它
        if (mPressedBgColorResId == INVALID_ID) {
            return super.onTouchEvent(event);
        }
        GradientDrawable gradientDrawable = (GradientDrawable) drawable;

        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                //按下时颜色
                gradientDrawable.setColor(SkinManager.getColorArgb(mPressedBgColor,
                        mPressedBgColorResId));
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                //抬起时颜色
                gradientDrawable.setColor(SkinManager.getColorArgb(mNormalBgColor, mNormalBgColorResId));
                break;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        Drawable background = getBackground();
        //三方库  无缓存的颜色
        if (background instanceof GradientDrawable) {
            GradientDrawable gradientDrawable = (GradientDrawable) background;
            if (enabled) {
                //正常颜色
                if (mNormalBgColor == INVALID_ID) {
                    gradientDrawable.setColor(mNormalBgColor);
                } else {
                    gradientDrawable.setColor(SkinManager.getColorArgb(mNormalBgColor,
                            mNormalBgColorResId));
                }
            } else {
                //禁用颜色
                gradientDrawable.setColor(SkinManager.getColorArgbByContext(getContext(), R.color
                        .custom_gray));
            }
        }

    }

}
