package com.xfhy.skin.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.tools.R;

import support.SkinManager;
import support.widget.SkinCompatView;

/**
 * Created by xfhy on 2018/7/25 17:28
 * Description : 支持换肤的开关控件
 *
 * 开启硬件加速才会显示阴影效果!  建议不开启,有点小丑
 */
public class SkinSwitch extends SkinCompatView {

    private static final int BORDER_WIDTH = 2;
    private static final int DEFAULT_PADDING = 5;

    private Paint mWhitePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint mBackGroundPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint mBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private Paint mLeftOvalBorderPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private boolean mIsChecked = false;
    private int mWidth;
    private int mHeight;

    /**
     * 灰色边框
     */
    private Path mOuterGrayPath;
    private int mBgColorResourceId;

    public SkinSwitch(Context context) {
        this(context, null);
    }

    public SkinSwitch(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.radioButtonStyle);
    }

    public SkinSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SkinSwitch);
        mBgColorResourceId = typedArray.getResourceId(R.styleable.SkinSwitch_checkedSkinSwitchBgColor, R.color.main_theme_color);
        mIsChecked = typedArray.getBoolean(R.styleable.SkinSwitch_SkinSwitchBgChecked,false);
        typedArray.recycle();

        initView();
    }

    private void initView() {
        mWhitePaint.setColor(Color.WHITE);
        mBorderPaint.setStyle(Paint.Style.STROKE);
        mBorderPaint.setStrokeWidth(BORDER_WIDTH);
        int grayColor = getResources().getColor(R.color.color_cdcdcd);
        mBorderPaint.setColor(grayColor);
        mLeftOvalBorderPaint.setStyle(Paint.Style.STROKE);
        mLeftOvalBorderPaint.setStrokeWidth(BORDER_WIDTH);
        mLeftOvalBorderPaint.setColor(grayColor);
//        mLeftOvalBorderPaint.setShadowLayer(5, 0, 3, grayColor);  //不要阴影,看起来有点小丑
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;

        //提前计算好未选中时的边框  避免在onDraw()中耗时
        mOuterGrayPath = new Path();
        RectF leftOval = new RectF(DEFAULT_PADDING, DEFAULT_PADDING, mHeight - DEFAULT_PADDING, mHeight - DEFAULT_PADDING);
        mOuterGrayPath.addArc(leftOval, -90, -180);
        mOuterGrayPath.moveTo(mHeight / 2, DEFAULT_PADDING);
        mOuterGrayPath.lineTo(mWidth - mHeight / 2, DEFAULT_PADDING);
        RectF rightHalfOval = new RectF(mWidth - mHeight, DEFAULT_PADDING, mWidth - DEFAULT_PADDING, mHeight - DEFAULT_PADDING);
        mOuterGrayPath.addArc(rightHalfOval, -90, 180);
        mOuterGrayPath.lineTo(mHeight / 2, mHeight - DEFAULT_PADDING);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (mIsChecked) {
            //画主题背景和白色圆   其实就是一根线条和一个白色的圆
            mBackGroundPaint.setColor(SkinManager.getColorArgbByContext(getContext(), mBgColorResourceId));
            mBackGroundPaint.setStrokeWidth(mHeight - 2 * DEFAULT_PADDING);
            mBackGroundPaint.setStrokeCap(Paint.Cap.ROUND);
            //线条的起点圆半径
            int radius = (mHeight - 2 * DEFAULT_PADDING) / 2;
            canvas.drawLine(DEFAULT_PADDING + radius, mHeight / 2, mWidth - DEFAULT_PADDING - radius, mHeight / 2, mBackGroundPaint);

            //白色圆 稍微离边界有点距离
            canvas.drawCircle(mWidth - DEFAULT_PADDING - radius, mHeight / 2, radius - 2, mWhitePaint);
        } else {
            //画灰色边框
            canvas.drawPath(mOuterGrayPath, mBorderPaint);

            canvas.drawCircle(mHeight / 2, mHeight / 2,
                    mHeight / 2 - DEFAULT_PADDING - mBorderPaint.getStrokeWidth() / 2, mLeftOvalBorderPaint);
        }
    }

    @Override
    public void applySkin() {
        super.applySkin();
        invalidate();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                mIsChecked = !mIsChecked;
                invalidate();
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                break;
            default:
                break;
        }
        return super.onTouchEvent(event);
    }

    public boolean isChecked() {
        return mIsChecked;
    }

    public void setChecked(boolean checked) {
        mIsChecked = checked;
        invalidate();
    }

}
