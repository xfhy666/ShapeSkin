package com.xfhy.skin.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;

import com.tools.R;
import com.tools.common.util.DisplayUtil;

/**
 * Created by xfhy on 2018/7/26 15:44
 * Description : 带有小圆点(消息)的TextSelectorRadioButton
 *
 * 可以控制小圆点显示和隐藏,并可控制小圆点颜色,大小
 */
public class TSRadioButtonWithPoint extends TextSelectorRadioButton {

    private boolean mEnablePoint;
    private float mCircleRadius;
    private Paint mCirclePaint = new Paint(Paint.ANTI_ALIAS_FLAG);
    private int mWidth;

    public TSRadioButtonWithPoint(Context context) {
        this(context, null);
    }

    public TSRadioButtonWithPoint(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.radioButtonStyle);
    }

    public TSRadioButtonWithPoint(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TSRadioButtonWithPoint);
        mEnablePoint = typedArray.getBoolean(R.styleable.TSRadioButtonWithPoint_TSRadioButtonPointEnable, false);
        int circleColor = typedArray.getColor(R.styleable.TSRadioButtonWithPoint_TSRadioButtonPointColor, Color.RED);
        mCircleRadius = typedArray.getDimension(R.styleable.TSRadioButtonWithPoint_TSRadioButtonPointRadius, DisplayUtil.dp2px(5));
        typedArray.recycle();
        mCirclePaint.setColor(circleColor);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (mEnablePoint) {
            canvas.drawCircle(mWidth / 2 + mCircleRadius * 2, getPaddingTop() + mCircleRadius * 2, mCircleRadius, mCirclePaint);
        }
    }

    public boolean isEnablePoint() {
        return mEnablePoint;
    }

    public void setEnablePoint(boolean enablePoint) {
        mEnablePoint = enablePoint;
        invalidate();
    }
}
