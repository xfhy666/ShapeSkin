package com.xfhy.skin.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.content.res.AppCompatResources;
import android.util.AttributeSet;

import com.tools.R;

import support.SkinManager;
import support.content.res.ColorState;
import support.content.res.SkinCompatUserThemeManager;
import support.widget.SkinCompatCheckBox;

import static support.widget.SkinCompatHelper.INVALID_ID;

/**
 * Created by xfhy on 2018/7/18 16:45
 * Description : SVG背景的CheckBox
 */
public class SvgBgCheckBox extends SkinCompatCheckBox {
    /*
     * 不能直接将SVG赋值给android:drawableLeft这种类似的属性,4.4以下会直接报错....只能自定义属性
     * */

    /**
     * 选中和未选中文字颜色
     */
    private int mCheckedTextColorResId;
    private int mNormalTextColorResId;
    private int mCheckedTextColor;
    private int mNormalTextColor;

    /**
     * 选中和未选中svg 颜色
     */
    private int mCheckedSvgColorResId;
    private int mNormalSvgColorResId;
    private int mCheckedSvgColor;
    private int mNormalSvgColor;

    //正常的drawable resId
    private int mNormalDrawableResId;
    //选中的drawable resId
    private int mCheckedDrawableResId;

    private Drawable mNormalDrawable;
    private Drawable mCheckedDrawable;

    public SvgBgCheckBox(Context context) {
        this(context, null);
    }

    public SvgBgCheckBox(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.checkboxStyle);
    }

    public SvgBgCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SvgBgCheckBox);

        //文字颜色
        mCheckedTextColorResId = typedArray.getResourceId(R.styleable.SvgBgCheckBox_checkedSvgBgCheckBoxTextColor, R.color
                .main_theme_color);
        mNormalTextColorResId = typedArray.getResourceId(R.styleable.SvgBgCheckBox_normalSvgBgCheckBoxTextColor, R.color.color_A9ABAB);
        mCheckedTextColor = typedArray.getColor(R.styleable.SvgBgCheckBox_checkedSvgBgCheckBoxTextColor,
                getResources().getColor(R.color.main_theme_color));
        mNormalTextColor = typedArray.getColor(R.styleable.SvgBgCheckBox_normalSvgBgCheckBoxTextColor,
                getResources().getColor(R.color.color_A9ABAB));

        //svg颜色
        mCheckedSvgColorResId = typedArray.getResourceId(R.styleable.SvgBgCheckBox_checkedSvgBgCheckBoxSvgColor, INVALID_ID);
        mNormalSvgColorResId = typedArray.getResourceId(R.styleable.SvgBgCheckBox_normalSvgBgCheckBoxSvgColor, INVALID_ID);
        mCheckedSvgColor = typedArray.getColor(R.styleable.SvgBgCheckBox_checkedSvgBgCheckBoxSvgColor, getResources()
                .getColor(R.color.main_theme_color));
        mNormalSvgColor = typedArray.getColor(R.styleable.SvgBgCheckBox_normalSvgBgCheckBoxSvgColor, getResources()
                .getColor(R.color.color_A9ABAB));


        mNormalDrawableResId = typedArray.getResourceId(R.styleable.SvgBgCheckBox_normalSvgBgCheckBoxDrawable, INVALID_ID);
        mCheckedDrawableResId = typedArray.getResourceId(R.styleable.SvgBgCheckBox_checkedSvgBgCheckBoxDrawable, INVALID_ID);
        //drawable 获取
        //5.0 是分界线  5.0上下获取svg drawable方式不同
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mNormalDrawable = typedArray.getDrawable(R.styleable.SvgBgCheckBox_normalSvgBgCheckBoxDrawable);
            mCheckedDrawable = typedArray.getDrawable(R.styleable.SvgBgCheckBox_checkedSvgBgCheckBoxDrawable);
        } else {
            if (mNormalDrawableResId != INVALID_ID) {
                mNormalDrawable = AppCompatResources.getDrawable(context, mNormalDrawableResId);
            }
            if (mCheckedDrawableResId != INVALID_ID) {
                mCheckedDrawable = AppCompatResources.getDrawable(context, mCheckedDrawableResId);
            }
        }
        dealWithDrawable(mNormalDrawable, true);
        dealWithDrawable(mCheckedDrawable, false);
        typedArray.recycle();
        skinChange();
    }

    private void skinChange() {
        boolean checked = isChecked();

        //--------------------------------处理文字---------------------------------------
        ColorState normalTextColor = SkinCompatUserThemeManager.get().getColorState(mNormalTextColorResId);
        ColorState checkedTextColor = SkinCompatUserThemeManager.get().getColorState(mCheckedTextColorResId);

        //文字颜色
        if (checked) {
            setTextColor(SkinManager.getColorArgb(mCheckedTextColor, checkedTextColor));
        } else {
            setTextColor(SkinManager.getColorArgb(mNormalTextColor, normalTextColor));
        }

        //drawable处理
        setBackground(isChecked() ? dealWithDrawable(mCheckedDrawable, false) : dealWithDrawable(mNormalDrawable, true));
    }

    @Override
    public void applySkin() {
        super.applySkin();
        skinChange();
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        skinChange();
    }

    /**
     * 处理svg drawable 的颜色
     */
    private Drawable dealWithDrawable(Drawable drawable, boolean isNormal) {
        if (drawable == null) {
            return null;
        }
        int color = 0;
        //有SVG颜色则使用  没有则使用文字的颜色
        if (mCheckedSvgColorResId == INVALID_ID) {
            color = SkinManager.getColorArgbByContext(getContext(), isNormal ? mNormalTextColorResId : mCheckedTextColorResId);
        } else {
            if (isNormal) {
                color = SkinManager.getColorArgbByContext(getContext(), mNormalSvgColorResId);
            } else {
                color = SkinManager.getColorArgbByContext(getContext(), mCheckedSvgColorResId);
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (drawable instanceof VectorDrawable) {
                VectorDrawable vectorDrawable = (VectorDrawable) drawable;
                vectorDrawable.setTint(color);
                return vectorDrawable;
            }
        } else {
            if (drawable instanceof VectorDrawableCompat) {
                VectorDrawableCompat vectorDrawableCompat = (VectorDrawableCompat) drawable;
                vectorDrawableCompat.setTint(color);
                return vectorDrawableCompat;
            }
        }
        return null;
    }

    @Override
    public void setBackgroundResource(int resId) {
        super.setBackgroundResource(resId);

        Drawable drawable = AppCompatResources.getDrawable(getContext(), resId);
        int color = SkinManager.getColorArgbByContext(getContext(), isChecked() ? mCheckedSvgColorResId : mNormalSvgColorResId);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (drawable instanceof VectorDrawable) {
                VectorDrawable vectorDrawable = (VectorDrawable) drawable;
                vectorDrawable.setTint(color);
            }
        } else {
            if (drawable instanceof VectorDrawableCompat) {
                VectorDrawableCompat vectorDrawableCompat = (VectorDrawableCompat) drawable;
                vectorDrawableCompat.setTint(color);
            }
        }
        setBackground(drawable);
    }
}
