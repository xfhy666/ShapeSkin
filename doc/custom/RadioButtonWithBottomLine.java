package com.xfhy.skin.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

import com.xfhy.skin.R;

import skin.support.content.res.ColorState;
import skin.support.content.res.SkinCompatUserThemeManager;
import skin.support.widget.SkinCompatRadioButton;


/**
 * Created by xfhy on 2018/7/6 13:44
 * Description : 定制的RadioButton,底部有根线
 * <p>
 * 自定义属性:
 * <p>
 * 正常时文字颜色
 * name="normalRBtnLineTextColor" format="color"/> <br/>
 * 选中时文字颜色
 * name="checkedRBtnLineTextColor" format="color"/><br/>
 * 线条高度
 * name="lineHeightRBtnLine" format="dimension"/><br/>
 */
public class RadioButtonWithBottomLine extends SkinCompatRadioButton {

    private final static int DEFAULT_PAINT_WIDTH = 3;
    private int mCheckedTextColor;
    private int mNormalTextColor;
    private int mCheckedTextColorResId;
    private int mNormalTextColorResId;
    private Paint mPaint;
    private int mWidth;
    private int mHeight;
    private int mLineHeight;

    public RadioButtonWithBottomLine(Context context) {
        this(context, null);
    }

    public RadioButtonWithBottomLine(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.radioButtonStyle);
    }

    public RadioButtonWithBottomLine(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.RadioButtonWithBottomLine);

        mCheckedTextColorResId = typedArray.getResourceId(R.styleable
                .RadioButtonWithBottomLine_checkedRBtnLineTextColor, R.color.main_theme_color);
        mNormalTextColorResId = typedArray.getResourceId(R.styleable
                .RadioButtonWithBottomLine_normalRBtnLineTextColor, R.color.color_A9ABAB);

        mCheckedTextColor = typedArray.getColor(R.styleable
                        .RadioButtonWithBottomLine_checkedRBtnLineTextColor,
                getResources().getColor(R.color.main_theme_color));
        mNormalTextColor = typedArray.getColor(R.styleable.RadioButtonWithBottomLine_normalRBtnLineTextColor,
                getResources().getColor(R.color.color_A9ABAB));
        //这里获取到的不是dp 获取到的是px值   因为getDimension()方法中将dp转化成了px
        mLineHeight = (int) typedArray.getDimension(R.styleable
                .RadioButtonWithBottomLine_lineHeightRBtnLine, DEFAULT_PAINT_WIDTH);
        typedArray.recycle();

        initView();

        skinChange();
    }

    private void initView() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setStrokeWidth(mLineHeight);
        ColorState checkedTextColor = SkinCompatUserThemeManager.get().getColorState(mCheckedTextColorResId);
        //文字颜色
        mPaint.setColor((SkinManager.getColorArgb(mCheckedTextColor, checkedTextColor)));
    }

    private void skinChange() {
        boolean checked = isChecked();
        ColorState normalTextColor = SkinCompatUserThemeManager.get().getColorState(mNormalTextColorResId);
        ColorState checkedTextColor = SkinCompatUserThemeManager.get().getColorState(mCheckedTextColorResId);

        //文字颜色
        if (checked) {
            setTextColor(SkinManager.getColorArgb(mCheckedTextColor, checkedTextColor));
        } else {
            setTextColor(SkinManager.getColorArgb(mNormalTextColor, normalTextColor));
        }
        invalidate();  //可能需要更新底部画线
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        ColorState checkedTextColor = SkinCompatUserThemeManager.get().getColorState(mCheckedTextColorResId);
        //文字颜色
        mPaint.setColor((SkinManager.getColorArgb(mCheckedTextColor, checkedTextColor)));
        if (isChecked()) {
            //画底部的线条
            canvas.drawLine(0, mHeight, mWidth, mHeight, mPaint);
        }
    }

    @Override
    public void applySkin() {
        super.applySkin();
        skinChange();
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        skinChange();
    }

}
