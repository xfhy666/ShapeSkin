package com.xfhy.skin.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.content.res.AppCompatResources;
import android.util.AttributeSet;

import com.tools.R;

import support.SkinManager;
import support.widget.SkinCompatImageView;

import static support.widget.SkinCompatHelper.INVALID_ID;


/**
 * Created by xfhy on 2018/7/6 8:18
 * Description : 支持SVG换肤的ImageView
 * <p>
 * <p>
 * <h1>!!!一级警告!!!</h1>
 * <b>必须使用app:srcCompat="@drawable/svg_phone_round"这种形式使用</b><br/>
 * <p>
 * 使用举例:
 * <pre>
 *     &lt;support.widget.custom.SVGImageView
 * android:layout_width="wrap_content"
 * android:layout_height="wrap_content"
 * app:srcCompat="@drawable/svg_icon_add_account_img"
 * app:SVGImageViewColor="@color/main_theme_color"
 * app:SVGImageViewDrawable="@drawable/svg_icon_add_account_img"/&gt;
 * </pre>
 */
public class SVGImageView extends SkinCompatImageView {

    private int mSVGColorResId;
    private int mSVGColor;
    private int mSVGResourceId;

    public SVGImageView(Context context) {
        this(context, null);
    }

    public SVGImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SVGImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SVGImageView);
        mSVGColorResId = typedArray.getResourceId(R.styleable
                .SVGImageView_SVGImageViewColor, R.color.main_theme_color);
        mSVGColor = typedArray.getColor(R.styleable.SVGImageView_SVGImageViewColor,
                getResources().getColor(R.color.main_theme_color));
        mSVGResourceId = typedArray.getResourceId(R.styleable.SVGImageView_SVGImageViewDrawable, INVALID_ID);

        typedArray.recycle();
        skinChange();
    }

    private void skinChange() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            Drawable drawable = getDrawable();
            //API 21以上的处理方式
            if (drawable instanceof VectorDrawable) {
                VectorDrawable vectorDrawable = (VectorDrawable) drawable;
                vectorDrawable.setTint(SkinManager.getColorArgb(mSVGColor, mSVGColorResId));
            }
        } else {
            //兼容API21 以下的手机
            if (mSVGResourceId == INVALID_ID) {
                return;
            }
            Drawable drawable = AppCompatResources.getDrawable(getContext(), mSVGResourceId);
            if (drawable != null && drawable instanceof VectorDrawableCompat) {
                VectorDrawableCompat vectorDrawableCompat = (VectorDrawableCompat) drawable;
                vectorDrawableCompat.setTint(SkinManager.getColorArgb(mSVGColor, mSVGColorResId));
                setImageDrawable(vectorDrawableCompat);
            }
        }

    }

    @Override
    public void applySkin() {
        super.applySkin();
        skinChange();
    }

    /**
     * 设置SVG资源和SVG颜色
     *
     * @param svgResId      SVG资源
     * @param svgColorResId SVG颜色
     */
    public void setSvgResAndColor(int svgResId, int svgColorResId) {
        mSVGResourceId = svgResId;
        mSVGColorResId = svgColorResId;
        if (mSVGResourceId == INVALID_ID) {
            return;
        }
        Drawable drawable = null;
        try {
            drawable = AppCompatResources.getDrawable(getContext(), mSVGResourceId);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (drawable != null && drawable instanceof VectorDrawable) {
                VectorDrawable vectorDrawable = (VectorDrawable) drawable;
                vectorDrawable.setTint(SkinManager.getColorArgbByContext(getContext(), mSVGColorResId));
                setImageDrawable(vectorDrawable);
            }
        } else {
            if (drawable != null && drawable instanceof VectorDrawableCompat) {
                VectorDrawableCompat vectorDrawableCompat = (VectorDrawableCompat) drawable;
                vectorDrawableCompat.setTint(SkinManager.getColorArgbByContext(getContext(), mSVGColorResId));
                setImageDrawable(vectorDrawableCompat);
            }
        }
    }

    /**
     * 设置SVG颜色
     *
     * @param svgColorResId 颜色
     */
    public void setSvgColor(int svgColorResId) {
        mSVGColorResId = svgColorResId;
        if (mSVGResourceId == INVALID_ID) {
            return;
        }
        Drawable background = getBackground();
        if (background != null && background instanceof VectorDrawableCompat) {
            VectorDrawableCompat vectorDrawableCompat = (VectorDrawableCompat) background;
            vectorDrawableCompat.setTint(SkinManager.getColorArgbByContext(getContext(), mSVGColorResId));
            setImageDrawable(vectorDrawableCompat);
        }
    }
}
