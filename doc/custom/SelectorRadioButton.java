package com.xfhy.skin.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;

import com.tools.R;

import support.SkinManager;
import support.content.res.ColorState;
import support.content.res.SkinCompatUserThemeManager;
import support.widget.SkinCompatRadioButton;

/**
 * Created by xfhy on 2018/7/5 14:12
 * Description : 自定义的RadioButton 可控制textColorSelector 和背景颜色selector
 */
public class SelectorRadioButton extends SkinCompatRadioButton {

    private int mCheckedTextColor;
    private int mNormalTextColor;
    private int mCheckedBgColor;
    private int mNormalBgColor;
    private int mCheckedTextColorResId;
    private int mNormalTextColorResId;
    private int mCheckedBgColorResId;
    private int mNormalBgColorResId;

    public SelectorRadioButton(Context context) {
        this(context, null);
    }

    public SelectorRadioButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.radioButtonStyle);
    }

    public SelectorRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SelectorRadioButton);

        mCheckedTextColorResId = typedArray.getResourceId(R.styleable
                .SelectorRadioButton_checkedTextColor, R.color.main_theme_color);
        mNormalTextColorResId = typedArray.getResourceId(R.styleable
                .SelectorRadioButton_normalTextColor, R.color.color_A9ABAB);
        mCheckedBgColorResId = typedArray.getResourceId(R.styleable
                .SelectorRadioButton_checkedBGColor, R.color.color_ffffff);
        mNormalBgColorResId = typedArray.getResourceId(R.styleable
                .SelectorRadioButton_normalBGColor, R.color.color_ffffff);

        mCheckedTextColor = typedArray.getColor(R.styleable.SelectorRadioButton_checkedTextColor,
                getResources().getColor(R.color.main_theme_color));
        mNormalTextColor = typedArray.getColor(R.styleable.SelectorRadioButton_normalTextColor,
                getResources().getColor(R.color.color_A9ABAB));
        mCheckedBgColor = typedArray.getColor(R.styleable.SelectorRadioButton_checkedBGColor,
                getResources().getColor(R.color.color_ffffff));
        mNormalBgColor = typedArray.getColor(R.styleable.SelectorRadioButton_normalBGColor,
                getResources().getColor(R.color.color_ffffff));
        typedArray.recycle();
        skinChange();
    }

    private void skinChange() {
        Drawable drawable = getBackground();
        boolean checked = isChecked();
        ColorState normalBG = SkinCompatUserThemeManager.get().getColorState(mNormalBgColorResId);
        ColorState checkedBG = SkinCompatUserThemeManager.get().getColorState(mCheckedBgColorResId);
        ColorState normalTextColor = SkinCompatUserThemeManager.get().getColorState(mNormalTextColorResId);
        ColorState checkedTextColor = SkinCompatUserThemeManager.get().getColorState(mCheckedTextColorResId);

        if (drawable instanceof GradientDrawable) {
            GradientDrawable gradientDrawable = (GradientDrawable) drawable;
            if (checked) {
                //选中 则设置选中的背景颜色
                gradientDrawable.setColor(SkinManager.getColorArgb(mCheckedBgColor, checkedBG));
            } else {
                //未选中
                gradientDrawable.setColor(SkinManager.getColorArgb(mNormalBgColor, normalBG));
            }
        } else {
            //未设置shape
            if (checked) {
                setBackgroundColor(SkinManager.getColorArgb(mCheckedBgColor, checkedBG));
            } else {
                setBackgroundColor(SkinManager.getColorArgb(mNormalBgColor, normalBG));
            }
        }

        //文字颜色
        if (checked) {
            setTextColor(SkinManager.getColorArgb(mCheckedTextColor, checkedTextColor));
        } else {
            setTextColor(SkinManager.getColorArgb(mNormalTextColor, normalTextColor));
        }

    }

    @Override
    public void applySkin() {
        super.applySkin();
        skinChange();
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        skinChange();
    }
}
