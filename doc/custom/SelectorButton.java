package com.xfhy.skin.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.tools.R;

import support.SkinManager;
import support.widget.SkinCompatButton;

/**
 * Created by xfhy on 2018年7月5日08:12:28
 * Description : 一个普通的Button,支持shape换肤,支持Selector
 */
public class SelectorButton extends SkinCompatButton {

    /**
     * 是否开启selector模式
     */
    //private boolean mIsOpenSelecMode = false;

    /**
     * 本地默认按下颜色
     */
    private int mPressedBgColor;
    /**
     * 本地默认正常颜色
     */
    private int mNormalBgColor;
    /**
     * 按下颜色的资源id
     */
    private int mPressedBgColorResId;
    /**
     * 正常颜色的资源id
     */
    private int mNormalBgColorResId;

    public SelectorButton(Context context) {
        super(context);
    }

    public SelectorButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.buttonStyle);
    }

    public SelectorButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        readCustomAttribute(context, attrs);
        skinChange();
    }

    private void readCustomAttribute(Context context, AttributeSet attrs) {
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SelectorButton);
        mPressedBgColorResId = typedArray.getResourceId(R.styleable
                .SelectorButton_pressedSelectorButtonBGColor, R.color.main_theme_color_pressed);
        mNormalBgColorResId = typedArray.getResourceId(R.styleable
                .SelectorButton_normalSelectorButtonBGColor, R.color.main_theme_color);

        mPressedBgColor = typedArray.getColor(R.styleable.SelectorButton_pressedSelectorButtonBGColor,
                getResources().getColor(R.color.main_theme_color_pressed));
        mNormalBgColor = typedArray.getColor(R.styleable.SelectorButton_normalSelectorButtonBGColor,
                getResources().getColor(R.color.main_theme_color));
        typedArray.recycle();
    }

    private void skinChange() {
        if(isEnabled()){
            Drawable drawable = getBackground();
            if (drawable instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) drawable;
                gradientDrawable.setColor(SkinManager.getColorArgb(mNormalBgColor, mNormalBgColorResId));
            }
        }
    }

    @Override
    public void applySkin() {
        if(isEnabled()){
            super.applySkin();
            skinChange();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //如果未开启selector模式,则直接返回
        /*if (!mIsOpenSelecMode) {
            return super.onTouchEvent(event);
        }*/

        Drawable drawable = getBackground();

        if (!isEnabled() || !(drawable instanceof GradientDrawable)) {
            return super.onTouchEvent(event);
        }
        GradientDrawable gradientDrawable = (GradientDrawable) drawable;

        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                //按下时颜色
                gradientDrawable.setColor(SkinManager.getColorArgb(mPressedBgColor,
                        mPressedBgColorResId));
                break;
            case MotionEvent.ACTION_MOVE:
                break;
            case MotionEvent.ACTION_UP:
                //抬起时颜色
                gradientDrawable.setColor(SkinManager.getColorArgb(mNormalBgColor, mNormalBgColorResId));
                break;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        Drawable background = getBackground();
        //三方库  无缓存的颜色
        if (background instanceof GradientDrawable) {
            GradientDrawable gradientDrawable = (GradientDrawable) background;
            if (enabled) {
                //正常颜色
                gradientDrawable.setColor(SkinManager.getColorArgb(mNormalBgColor, mNormalBgColorResId));
            } else {
                //禁用颜色
                gradientDrawable.setColor(SkinManager.getColorArgbByContext(getContext(), R.color
                        .custom_gray));
            }
        }

    }
}
