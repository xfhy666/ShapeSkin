package com.xfhy.skin.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.content.res.AppCompatResources;
import android.util.AttributeSet;

import com.tools.R;

import support.SkinManager;
import support.content.res.ColorState;
import support.content.res.SkinCompatUserThemeManager;
import support.widget.SkinCompatRadioButton;

import static support.widget.SkinCompatHelper.INVALID_ID;

/**
 * Created by xfhy on 2018/7/6 16:58
 * Description : 自定义的RadioButton
 * <p>
 * <li>支持textColorSelector</li>
 * <li>支持设置上下左右drawable</li>
 * <li>可以svg Drawable换肤</li>
 */
public class TextSelectorRadioButton extends SkinCompatRadioButton {

    /*
     * 不能直接将SVG赋值给android:drawableLeft这种类似的属性,4.4以下会直接报错....只能自定义属性
     * */

    /**
     * 选中和未选中文字颜色
     */
    private int mCheckedTextColorResId;
    private int mNormalTextColorResId;
    private int mCheckedTextColor;
    private int mNormalTextColor;

    /**
     * 选中和未选中svg 颜色
     */
    private int mCheckedSvgColorResId;
    private int mNormalSvgColorResId;
    private int mCheckedSvgColor;
    private int mNormalSvgColor;

    /**
     * 上下左右drawable resId
     */
    private int mSVGResourceLeftId;
    private int mSVGResourceTopId;
    private int mSVGResourceRightId;
    private int mSVGResourceBottomId;

    //一般来说 一个RadioButton 只会同时设置一个drawable
    //正常的drawable resId
    private int mNormalDrawableResId;
    //选中的drawable resId
    private int mCheckedDrawableResId;

    /**
     * 上下左右drawable
     */
    private Drawable mLeftDrawable;
    private Drawable mTopDrawable;
    private Drawable mRightDrawable;
    private Drawable mBottomDrawable;
    private Drawable mNormalDrawable;
    private Drawable mCheckedDrawable;

    public TextSelectorRadioButton(Context context) {
        this(context, null);
    }

    public TextSelectorRadioButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.support.v7.appcompat.R.attr.radioButtonStyle);
    }

    public TextSelectorRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TextSelectorRadioButton);

        //文字颜色
        mCheckedTextColorResId = typedArray.getResourceId(R.styleable.TextSelectorRadioButton_checkedTextSRBTextColor, R.color
                .main_theme_color);
        mNormalTextColorResId = typedArray.getResourceId(R.styleable.TextSelectorRadioButton_normalTextSRBTextColor, R.color.color_A9ABAB);
        mCheckedTextColor = typedArray.getColor(R.styleable.TextSelectorRadioButton_checkedTextSRBTextColor,
                getResources().getColor(R.color.main_theme_color));
        mNormalTextColor = typedArray.getColor(R.styleable.TextSelectorRadioButton_normalTextSRBTextColor,
                getResources().getColor(R.color.color_A9ABAB));

        //svg颜色
        mCheckedSvgColorResId = typedArray.getResourceId(R.styleable.TextSelectorRadioButton_checkedTextSRBSvgColor, INVALID_ID);
        mNormalSvgColorResId = typedArray.getResourceId(R.styleable.TextSelectorRadioButton_normalTextSRBSvgColor, INVALID_ID);
        mCheckedSvgColor = typedArray.getColor(R.styleable.TextSelectorRadioButton_checkedTextSRBSvgColor, getResources()
                .getColor(R.color.main_theme_color));
        mNormalSvgColor = typedArray.getColor(R.styleable.TextSelectorRadioButton_normalTextSRBSvgColor, getResources()
                .getColor(R.color.color_A9ABAB));


        mSVGResourceLeftId = typedArray.getResourceId(R.styleable.TextSelectorRadioButton_drawableCompatLeft, INVALID_ID);
        mSVGResourceTopId = typedArray.getResourceId(R.styleable.TextSelectorRadioButton_drawableCompatTop, INVALID_ID);
        mSVGResourceRightId = typedArray.getResourceId(R.styleable.TextSelectorRadioButton_drawableCompatRight, INVALID_ID);
        mSVGResourceBottomId = typedArray.getResourceId(R.styleable.TextSelectorRadioButton_drawableCompatBottom, INVALID_ID);
        mNormalDrawableResId = typedArray.getResourceId(R.styleable.TextSelectorRadioButton_normalTextSRBDrawable, INVALID_ID);
        mCheckedDrawableResId = typedArray.getResourceId(R.styleable.TextSelectorRadioButton_checkedTextSRBDrawable, INVALID_ID);
        //drawable 获取
        //5.0 是分界线  5.0上下获取svg drawable方式不同
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mLeftDrawable = typedArray.getDrawable(R.styleable.TextSelectorRadioButton_drawableCompatLeft);
            mTopDrawable = typedArray.getDrawable(R.styleable.TextSelectorRadioButton_drawableCompatTop);
            mRightDrawable = typedArray.getDrawable(R.styleable.TextSelectorRadioButton_drawableCompatRight);
            mBottomDrawable = typedArray.getDrawable(R.styleable.TextSelectorRadioButton_drawableCompatBottom);
            mNormalDrawable = typedArray.getDrawable(R.styleable.TextSelectorRadioButton_normalTextSRBDrawable);
            mCheckedDrawable = typedArray.getDrawable(R.styleable.TextSelectorRadioButton_checkedTextSRBDrawable);
        } else {
            if (mSVGResourceLeftId != INVALID_ID) {
                mLeftDrawable = AppCompatResources.getDrawable(context, mSVGResourceLeftId);
            }
            if (mSVGResourceTopId != INVALID_ID) {
                mTopDrawable = AppCompatResources.getDrawable(context, mSVGResourceTopId);
            }
            if (mSVGResourceRightId != INVALID_ID) {
                mRightDrawable = AppCompatResources.getDrawable(context, mSVGResourceRightId);
            }
            if (mSVGResourceBottomId != INVALID_ID) {
                mBottomDrawable = AppCompatResources.getDrawable(context, mSVGResourceBottomId);
            }
            if (mNormalDrawableResId != INVALID_ID) {
                mNormalDrawable = AppCompatResources.getDrawable(context, mNormalDrawableResId);
            }
            if (mCheckedDrawableResId != INVALID_ID) {
                mCheckedDrawable = AppCompatResources.getDrawable(context, mCheckedDrawableResId);
            }
        }
        dealWithDrawable(mNormalDrawable, true);
        typedArray.recycle();
        skinChange();
    }

    private void skinChange() {
        boolean checked = isChecked();

        //--------------------------------处理文字---------------------------------------
        ColorState normalTextColor = SkinCompatUserThemeManager.get().getColorState(mNormalTextColorResId);
        ColorState checkedTextColor = SkinCompatUserThemeManager.get().getColorState(mCheckedTextColorResId);

        //文字颜色
        if (checked) {
            setTextColor(SkinManager.getColorArgb(mCheckedTextColor, checkedTextColor));
        } else {
            setTextColor(SkinManager.getColorArgb(mNormalTextColor, normalTextColor));
        }

        //----------------------------------处理Drawable---------------------------------
        //判断用户究竟是使用的哪个drawable
        if (mLeftDrawable != null && mSVGResourceLeftId != INVALID_ID && mNormalDrawable != null) {
            //选中择设置这个位置该有的resId   否则设置未选中的样式;
            mLeftDrawable = isChecked() ? AppCompatResources.getDrawable(getContext(), mSVGResourceLeftId) : mNormalDrawable;
        }
        if (mTopDrawable != null && mSVGResourceTopId != INVALID_ID && mNormalDrawable != null) {
            mTopDrawable = isChecked() ? AppCompatResources.getDrawable(getContext(), mSVGResourceTopId) : mNormalDrawable;
        }
        if (mRightDrawable != null && mSVGResourceRightId != INVALID_ID && mNormalDrawable != null) {
            mRightDrawable = isChecked() ? AppCompatResources.getDrawable(getContext(), mSVGResourceRightId) : mNormalDrawable;
        }
        if (mBottomDrawable != null && mSVGResourceBottomId != INVALID_ID && mNormalDrawable != null) {
            mBottomDrawable = isChecked() ? AppCompatResources.getDrawable(getContext(), mSVGResourceBottomId) : mNormalDrawable;
        }

        //5.0 是分界线
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            super.setCompoundDrawablesRelativeWithIntrinsicBounds(dealWithDrawable(mLeftDrawable, false),
                    dealWithDrawable(mTopDrawable, false),
                    dealWithDrawable(mRightDrawable, false), dealWithDrawable(mBottomDrawable, false));     // 4.4.4 可用
        } else {
            super.setCompoundDrawablesWithIntrinsicBounds(dealWithDrawable(mLeftDrawable, false),
                    dealWithDrawable(mTopDrawable, false), dealWithDrawable(mRightDrawable, false),
                    dealWithDrawable(mBottomDrawable, false));
        }
    }

    @Override
    public void applySkin() {
        super.applySkin();
        skinChange();
    }

    @Override
    public void setChecked(boolean checked) {
        super.setChecked(checked);
        skinChange();
    }

    /**
     * 处理svg drawable 的颜色
     */
    private Drawable dealWithDrawable(Drawable drawable, boolean isNormal) {
        if (drawable == null) {
            return null;
        }
        int color = 0;
        //有SVG颜色则使用  没有则使用文字的颜色
        if (mCheckedSvgColorResId == INVALID_ID) {
            color = SkinManager.getColorArgbByContext(getContext(), isChecked() ? mCheckedTextColorResId : mNormalTextColorResId);
        } else {
            if (isNormal) {
                color = SkinManager.getColorArgbByContext(getContext(), mNormalSvgColorResId);
            } else {
                color = SkinManager.getColorArgbByContext(getContext(), isChecked() ? mCheckedSvgColorResId : mNormalSvgColorResId);
            }
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (drawable instanceof VectorDrawable) {
                VectorDrawable vectorDrawable = (VectorDrawable) drawable;
                vectorDrawable.setTint(color);
                return vectorDrawable;
            }
        } else {
            if (drawable instanceof VectorDrawableCompat) {
                VectorDrawableCompat vectorDrawableCompat = (VectorDrawableCompat) drawable;
                vectorDrawableCompat.setTint(color);
                return vectorDrawableCompat;
            }
        }
        return null;
    }

    @Override
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(int start, int top, int end, int bottom) {
        super.setCompoundDrawablesRelativeWithIntrinsicBounds(start, top, end, bottom);
        setRoundDrawable(start, top, end, bottom);
    }

    @Override
    public void setCompoundDrawablesWithIntrinsicBounds(int left, int top, int right, int bottom) {
        super.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
        setRoundDrawable(left, top, right, bottom);
    }

    /**
     * 设置四周的drawable
     */
    private void setRoundDrawable(@DrawableRes int start, @DrawableRes int top, @DrawableRes int end, @DrawableRes int bottom) {
        mSVGResourceLeftId = start;
        mSVGResourceTopId = top;
        mSVGResourceRightId = end;
        mSVGResourceBottomId = bottom;
        createVectorDrawableCompat();
        setCompoundDrawables(mLeftDrawable, mTopDrawable, mRightDrawable, mBottomDrawable);
    }

    /**
     * 根据ResourceId创建VectorDrawableCompat
     * 并设置好颜色
     */
    private void createVectorDrawableCompat() {
        mLeftDrawable = (mSVGResourceLeftId == INVALID_ID ? null : AppCompatResources.getDrawable(getContext(), mSVGResourceLeftId));
        mTopDrawable = (mSVGResourceTopId == INVALID_ID ? null : AppCompatResources.getDrawable(getContext(), mSVGResourceTopId));
        mRightDrawable = (mSVGResourceRightId == INVALID_ID ? null : AppCompatResources.getDrawable(getContext(), mSVGResourceRightId));
        mBottomDrawable = (mSVGResourceBottomId == INVALID_ID ? null : AppCompatResources.getDrawable(getContext(), mSVGResourceBottomId));
        setBounds(mLeftDrawable);
        setBounds(mTopDrawable);
        setBounds(mRightDrawable);
        setBounds(mBottomDrawable);
        dealWithDrawable(mLeftDrawable,false);
        dealWithDrawable(mTopDrawable,false);
        dealWithDrawable(mRightDrawable,false);
        dealWithDrawable(mBottomDrawable,false);
    }

    private void setBounds(Drawable drawable) {
        if (drawable == null) {
            return;
        }
        drawable.setBounds(0, 0, drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight());
    }

}
