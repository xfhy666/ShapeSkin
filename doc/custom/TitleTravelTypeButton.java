package com.xfhy.skin.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.tools.R;

import java.util.ArrayList;

import support.SkinManager;
import support.widget.SkinCompatRadioButton;
import support.widget.SkinCompatRadioGroup;


/**
 * Created by xfhy on 2018年7月6日09:20:02
 * Description : 自定义顶部标题栏的因公因私按钮   从布局加载
 */
public class TitleTravelTypeButton extends SkinCompatRadioGroup implements RadioGroup
        .OnCheckedChangeListener {

    private OnCheckChangedListener mListener;
    private SkinCompatRadioButton mLeftBtn;
    private SkinCompatRadioButton mRightBtn;
    private RadioGroup mRootView;

    /**
     * 开启透明
     */
    private boolean mOpenedTransparent = false;

    public TitleTravelTypeButton(Context context) {
        this(context, null);
    }

    public TitleTravelTypeButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.TitleTravelTypeButton);
        mOpenedTransparent = typedArray.getBoolean(R.styleable.TitleTravelTypeButton_openTitleButtonTransparent, false);
        typedArray.recycle();

        initView();
        skinChange();
    }

    private void initView() {
        View.inflate(getContext(), R.layout.layout_title_travel_type, this);

        mRootView = findViewById(R.id.rg_root_title);
        mLeftBtn = findViewById(R.id.btn_public_title);
        mRightBtn = findViewById(R.id.btn_private_title);
        mRootView.setOnCheckedChangeListener(this);

        //设置背景是透明
        if (mOpenedTransparent) {
            Drawable rootViewBackground = mRootView.getBackground();
            if (rootViewBackground instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) rootViewBackground;
                gradientDrawable.setColor(getResources().getColor(R.color.color_00ffffff));
                //可以控制边框颜色 gradientDrawable.setStroke(DisplayUtil.dp2px(1),mainThemeColor);
            }
        }
    }

    private void skinChange() {
        Drawable leftDrawable = mLeftBtn.getBackground();
        Drawable rightDrawable = mRightBtn.getBackground();
        Drawable rootViewBackground = mRootView.getBackground();

        int mainThemeColor = SkinManager.getColorArgbByContext(getContext(), R.color
                .main_theme_color);
        int checkedColor = SkinManager.getColorArgbByContext(getContext(), R.color
                .due_to_public_choice);
        if (rootViewBackground instanceof GradientDrawable) {
            GradientDrawable gradientDrawable = (GradientDrawable) rootViewBackground;
            gradientDrawable.setColor(mOpenedTransparent ? getResources().getColor(R.color.color_00ffffff) : mainThemeColor);
            //可以控制边框颜色 gradientDrawable.setStroke(DisplayUtil.dp2px(1),mainThemeColor);
        }

        if (mLeftBtn.isChecked()) { //选中的是左边按钮
            if (leftDrawable instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) leftDrawable;
                gradientDrawable.setColor(checkedColor);
            }
            if (rightDrawable instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) rightDrawable;
                gradientDrawable.setColor(mOpenedTransparent ? getResources().getColor(R.color.color_00ffffff) : mainThemeColor);
            }

            //文字颜色
            mLeftBtn.setTextColor(mainThemeColor);
            mRightBtn.setTextColor(checkedColor);
        } else {   //选中的是右边按钮
            if (leftDrawable instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) leftDrawable;
                gradientDrawable.setColor(mOpenedTransparent ? getResources().getColor(R.color.color_00ffffff) : mainThemeColor);
            }
            if (rightDrawable instanceof GradientDrawable) {
                GradientDrawable gradientDrawable = (GradientDrawable) rightDrawable;
                gradientDrawable.setColor(checkedColor);
            }

            //文字颜色
            mLeftBtn.setTextColor(checkedColor);
            mRightBtn.setTextColor(mainThemeColor);
        }

    }

    @Override
    public void onCheckedChanged(RadioGroup group, int checkedId) {
        if (checkedId == R.id.btn_public_title) {
            leftBtnClick(mLeftBtn);
        } else if (checkedId == R.id.btn_private_title) {
            rightBtnClick(mRightBtn);
        }
        skinChange();
    }

    private void leftBtnClick(RadioButton view) {
        if (mListener != null) {
            mListener.checkChanged(view, true);
        }
    }

    private void rightBtnClick(RadioButton view) {
        if (mListener != null) {
            mListener.checkChanged(view, false);
        }
    }

    /**
     * 选中因公按钮
     */
    public void setPublicBtnChecked() {
        mLeftBtn.setChecked(true);
        mRightBtn.setChecked(false);
    }

    /**
     * 选中因私按钮
     */
    public void setPrivateBtnChecked() {
        mLeftBtn.setChecked(false);
        mRightBtn.setChecked(true);
    }

    /**
     * 设置单选文字
     *
     * @param array 文字list
     */
    public void setBarList(ArrayList<String> array) {
        if (array != null && array.size() == 2) {
            mLeftBtn.setText(array.get(0));
            mRightBtn.setText(array.get(1));
        }
    }

    @Override
    public void applySkin() {
        super.applySkin();
        skinChange();
    }

    public void setOnCheckChangedListener(OnCheckChangedListener listener) {
        mListener = listener;
    }

    public interface OnCheckChangedListener {
        /**
         * 选中状态改变
         *
         * @param isLeftChecked 左边是否选中
         */
        void checkChanged(RadioButton radioButton, boolean isLeftChecked);
    }

}
