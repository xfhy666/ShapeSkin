package com.xfhy.skin.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

import com.tools.R;
import com.tools.common.util.DisplayUtil;

import support.SkinManager;
import support.widget.SkinCompatView;

/**
 * Created by xfhy on 2018/7/9 15:45
 * Description : 圆形的View  可换肤
 */
public class CircleView extends SkinCompatView {

    private final static int DEFAULT_CIRCLE_RADIUS = DisplayUtil.dp2px(4);
    private Paint mPaint;
    private int mSolidColorResId;
    private int mSolidColor;
    private int mCircleRadius;
    private int mWidth;
    private int mHeight;

    public CircleView(Context context) {
        this(context, null);
    }

    public CircleView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.CircleView);
        mSolidColorResId = typedArray.getResourceId(R.styleable
                .CircleView_CircleViewSolidColor, R.color.main_theme_color);
        mSolidColor = typedArray.getColor(R.styleable.CircleView_CircleViewSolidColor,
                getResources().getColor(R.color.main_theme_color));
        mCircleRadius = (int) typedArray.getDimension(R.styleable.CircleView_CircleViewCircleRadius,
                DEFAULT_CIRCLE_RADIUS);
        typedArray.recycle();


        initView();
    }

    private void initView() {
        mPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        mPaint.setColor(SkinManager.getColorArgb(mSolidColor, mSolidColorResId));
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mWidth = w;
        mHeight = h;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        mPaint.setColor(SkinManager.getColorArgb(mSolidColor, mSolidColorResId));
        canvas.drawCircle(mWidth >> 1, mHeight >> 1, mCircleRadius, mPaint);
    }

    @Override
    public void applySkin() {
        super.applySkin();
        invalidate();
    }
}
