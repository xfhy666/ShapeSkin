package com.xfhy.skin.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.VectorDrawable;
import android.os.Build;
import android.support.annotation.DrawableRes;
import android.support.graphics.drawable.VectorDrawableCompat;
import android.support.v7.content.res.AppCompatResources;
import android.util.AttributeSet;

import com.tools.R;

import support.SkinManager;
import support.widget.SkinCompatTextView;

import static support.widget.SkinCompatHelper.INVALID_ID;

/**
 * Created by xfhy on 2018/7/13 8:38
 * Description : 支持上下左右SVG图片换肤的TextView
 */
public class SVGTextView extends SkinCompatTextView {

    private int mSVGColorResId;
    private int mSVGColor;

    private int mSVGResourceLeftId;
    private int mSVGResourceTopId;
    private int mSVGResourceRightId;
    private int mSVGResourceBottomId;

    private Drawable mLeftDrawable;
    private Drawable mTopDrawable;
    private Drawable mRightDrawable;
    private Drawable mBottomDrawable;

    public SVGTextView(Context context) {
        this(context, null);
    }

    public SVGTextView(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.textViewStyle);
    }

    public SVGTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.SVGTextView);
        //不能和文字颜色一样
        mSVGColorResId = typedArray.getResourceId(R.styleable.SVGTextView_SVGTextViewColor, R.color.main_theme_color);
        mSVGColor = typedArray.getColor(R.styleable.SVGTextView_SVGTextViewColor, getResources().getColor(R.color.main_theme_color));

        //drawable 获取
        //5.0 是分界线  5.0上下获取svg drawable方式不同
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mLeftDrawable = typedArray.getDrawable(R.styleable.SVGTextView_drawableSVGTextViewLeft);
            mTopDrawable = typedArray.getDrawable(R.styleable.SVGTextView_drawableSVGTextViewTop);
            mRightDrawable = typedArray.getDrawable(R.styleable.SVGTextView_drawableSVGTextViewRight);
            mBottomDrawable = typedArray.getDrawable(R.styleable.SVGTextView_drawableSVGTextViewBottom);
        } else {
            mSVGResourceLeftId = typedArray.getResourceId(R.styleable.SVGTextView_drawableSVGTextViewLeft, INVALID_ID);
            mSVGResourceTopId = typedArray.getResourceId(R.styleable.SVGTextView_drawableSVGTextViewTop, INVALID_ID);
            mSVGResourceRightId = typedArray.getResourceId(R.styleable.SVGTextView_drawableSVGTextViewRight, INVALID_ID);
            mSVGResourceBottomId = typedArray.getResourceId(R.styleable.SVGTextView_drawableSVGTextViewBottom, INVALID_ID);

            if (mSVGResourceLeftId != INVALID_ID) {
                mLeftDrawable = AppCompatResources.getDrawable(context, mSVGResourceLeftId);
            }
            if (mSVGResourceTopId != INVALID_ID) {
                mTopDrawable = AppCompatResources.getDrawable(context, mSVGResourceTopId);
            }
            if (mSVGResourceRightId != INVALID_ID) {
                mRightDrawable = AppCompatResources.getDrawable(context, mSVGResourceRightId);
            }
            if (mSVGResourceBottomId != INVALID_ID) {
                mBottomDrawable = AppCompatResources.getDrawable(context, mSVGResourceBottomId);
            }
        }

        typedArray.recycle();
        skinChange();
    }

    private void skinChange() {
        //----------------------------------处理Drawable---------------------------------
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            setCompoundDrawablesRelativeWithIntrinsicBounds(dealWithDrawable(mLeftDrawable), dealWithDrawable(mTopDrawable),
                    dealWithDrawable(mRightDrawable), dealWithDrawable(mBottomDrawable));     // 4.4.4 可用
        } else {
            setCompoundDrawablesWithIntrinsicBounds(dealWithDrawable(mLeftDrawable), dealWithDrawable(mTopDrawable),
                    dealWithDrawable(mRightDrawable), dealWithDrawable(mBottomDrawable));
        }
    }

    @Override
    public void setCompoundDrawablesRelativeWithIntrinsicBounds(int start, int top, int end, int bottom) {
        super.setCompoundDrawablesRelativeWithIntrinsicBounds(start, top, end, bottom);
        setRoundDrawable(start, top, end, bottom);
    }

    @Override
    public void setCompoundDrawablesWithIntrinsicBounds(int left, int top, int right, int bottom) {
        super.setCompoundDrawablesWithIntrinsicBounds(left, top, right, bottom);
        setRoundDrawable(left, top, right, bottom);
    }

    /**
     * 设置SVG颜色的resId
     */
    public void setSVGColorResId(int SVGColorResId) {
        mSVGColorResId = SVGColorResId;
    }

    @Override
    public void applySkin() {
        super.applySkin();
        skinChange();
    }

    /**
     * 处理svg drawable 的颜色 与文字颜色保持一致
     */
    private Drawable dealWithDrawable(Drawable drawable) {
        if (drawable == null) {
            return null;
        }
        int color = SkinManager.getColorArgbByContext(getContext(), mSVGColorResId);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            if (drawable instanceof VectorDrawable) {
                VectorDrawable vectorDrawable = (VectorDrawable) drawable;
                vectorDrawable.setTint(color);
                return vectorDrawable;
            }
        } else {
            if (drawable instanceof VectorDrawableCompat) {
                VectorDrawableCompat vectorDrawableCompat = (VectorDrawableCompat) drawable;
                vectorDrawableCompat.setTint(color);
                return vectorDrawableCompat;
            }
        }
        return null;
    }

    /**
     * 设置四周的drawable
     */
    private void setRoundDrawable(@DrawableRes int start, @DrawableRes int top, @DrawableRes int end, @DrawableRes int bottom) {
        mSVGResourceLeftId = start;
        mSVGResourceTopId = top;
        mSVGResourceRightId = end;
        mSVGResourceBottomId = bottom;
        createVectorDrawableCompat();
        setCompoundDrawables(mLeftDrawable, mTopDrawable, mRightDrawable, mBottomDrawable);
    }

    /**
     * 根据ResourceId创建VectorDrawableCompat
     * 并设置好颜色
     */
    private void createVectorDrawableCompat() {
        mLeftDrawable = (mSVGResourceLeftId == INVALID_ID ? null : AppCompatResources.getDrawable(getContext(), mSVGResourceLeftId));
        mTopDrawable = (mSVGResourceTopId == INVALID_ID ? null : AppCompatResources.getDrawable(getContext(), mSVGResourceTopId));
        mRightDrawable = (mSVGResourceRightId == INVALID_ID ? null : AppCompatResources.getDrawable(getContext(), mSVGResourceRightId));
        mBottomDrawable = (mSVGResourceBottomId == INVALID_ID ? null : AppCompatResources.getDrawable(getContext(), mSVGResourceBottomId));

        if (mLeftDrawable != null) {
            mLeftDrawable.setBounds(0, 0, mLeftDrawable.getIntrinsicWidth(), mLeftDrawable.getIntrinsicHeight());
            mLeftDrawable = dealWithDrawable(mLeftDrawable);
        }
        if (mTopDrawable != null) {
            mTopDrawable.setBounds(0, 0, mTopDrawable.getIntrinsicWidth(), mTopDrawable.getIntrinsicHeight());
            mTopDrawable = dealWithDrawable(mTopDrawable);
        }
        if (mRightDrawable != null) {
            mRightDrawable.setBounds(0, 0, mRightDrawable.getIntrinsicWidth(), mRightDrawable.getIntrinsicHeight());
            mRightDrawable = dealWithDrawable(mRightDrawable);
        }
        if (mBottomDrawable != null) {
            mBottomDrawable.setBounds(0, 0, mBottomDrawable.getIntrinsicWidth(), mBottomDrawable.getIntrinsicHeight());
            mBottomDrawable = dealWithDrawable(mBottomDrawable);
        }
    }

}
