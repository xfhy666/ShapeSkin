package com.xfhy.skin.custom;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.util.AttributeSet;

import com.tools.R;

import support.SkinManager;
import support.widget.SkinCompatButton;

/**
 * Created by xfhy on 2018/7/5 18:41
 * Description : 空心的按钮  可动态设置边框颜色
 * <p>
 * 需要设置background为shape
 */
public class HollowButton extends SkinCompatButton {

    public static final int DEFAULT_BORDER_WIDTH = 1;
    private int mBorderColorResId;
    private int mBorderColor;
    private int mBorderWidth;

    public HollowButton(Context context) {
        super(context);
    }

    public HollowButton(Context context, AttributeSet attrs) {
        this(context, attrs, android.R.attr.textViewStyle);
    }

    public HollowButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.HollowButton);
        mBorderColorResId = typedArray.getResourceId(R.styleable
                .HollowButton_HollowButtonBorderColor, R.color.main_theme_color);
        mBorderColor = typedArray.getColor(R.styleable.HollowButton_HollowButtonBorderColor,
                getResources().getColor(R.color.main_theme_color));
        mBorderWidth = (int) typedArray.getDimension(R.styleable.HollowButton_HollowButtonBorderWidth,
                DEFAULT_BORDER_WIDTH);
        typedArray.recycle();
        skinChange();
    }

    private void skinChange() {
        Drawable drawable = getBackground();
        if (drawable instanceof GradientDrawable) {
            GradientDrawable gradientDrawable = (GradientDrawable) drawable;
            gradientDrawable.setStroke(mBorderWidth, SkinManager.getColorArgb
                    (mBorderColor, mBorderColorResId));
        }
    }

    @Override
    public void applySkin() {
        super.applySkin();
        skinChange();
    }
}
