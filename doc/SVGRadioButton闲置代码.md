```md
private Drawable setSVGDrawable(Drawable drawable, int resourceTopCompatId) {
        if (drawable == null) {
            return null;
        }
        ColorState colorState = SkinCompatUserThemeManager.get().getColorState(R.color.colorAccent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            //API 21(Android 5.0)以上的处理方式   这里获取到的不是VectorDrawable
            if (drawable instanceof VectorDrawable) {
                VectorDrawable vectorDrawable = (VectorDrawable) drawable;
                if (colorState == null) {
                    vectorDrawable.setTint(getResources().getColor(R.color.colorAccent));
                } else {
                    vectorDrawable.setTint(Color.parseColor(colorState.getColorDefault()));
                }
                return vectorDrawable;
            }
        } else {
            //兼容API21 以下的手机
            //这里创建的vectorDrawableCompat可能为空
            /*VectorDrawableCompat vectorDrawableCompat = VectorDrawableCompat.create(getResources(),
                    resourceTopCompatId,
                    getContext().getTheme());
            if (vectorDrawableCompat != null) {
                if (colorState == null) {
                    vectorDrawableCompat.setTint(getResources().getColor(R.color.colorAccent));
                } else {
                    vectorDrawableCompat.setTint(Color.parseColor(colorState.getColorDefault()));
                }
                return vectorDrawableCompat;
            }*/

            //为了兼容那些上面创建VectorDrawableCompat时为空的机子
            /*Drawable drawableAgain = AppCompatResources.getDrawable(getContext(), resourceTopCompatId);
            if (drawableAgain != null && drawableAgain instanceof VectorDrawableCompat) {
                VectorDrawableCompat vectorDrawableCompatAgain = (VectorDrawableCompat) drawableAgain;
                if (colorState == null) {
                    vectorDrawableCompatAgain.setTint(getResources().getColor(R.color.colorAccent));
                } else {
                    vectorDrawableCompatAgain.setTint(Color.parseColor(colorState.getColorDefault()));
                }
                return vectorDrawableCompatAgain;
            }
            return drawableAgain;*/
        }
        return null;
    }




    private Drawable createSVGDrawable(int resourceId) {
            if (resourceId == -1) {
                return null;
            }
            ColorState colorState = SkinCompatUserThemeManager.get().getColorState(R.color.colorAccent);
            //兼容API21 以下的手机
            Drawable drawableCompat = AppCompatResources.getDrawable(getContext(), resourceId);
            if (drawableCompat == null) {
                return null;
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                //API 21 及以上   就是VectorDrawable
                if (drawableCompat instanceof VectorDrawable) {
                    VectorDrawable vectorDrawable = (VectorDrawable) drawableCompat;
                    if (colorState == null) {
                        vectorDrawable.setTint(getResources().getColor(R.color.colorAccent));
                    } else {
                        vectorDrawable.setTint(Color.parseColor(colorState.getColorDefault()));
                    }
                    return vectorDrawable;
                }
            } else {
                //API 21 以下  走的这里
                if (drawableCompat instanceof VectorDrawableCompat) {
                    VectorDrawableCompat vectorDrawableCompat = (VectorDrawableCompat) drawableCompat;
                    if (colorState == null) {
                        vectorDrawableCompat.setTint(getResources().getColor(R.color.colorAccent));
                    } else {
                        vectorDrawableCompat.setTint(Color.parseColor(colorState.getColorDefault()));
                    }
                    return vectorDrawableCompat;
                }
            }

            return drawableCompat;
        }
```